Advanced SIMD ("neon").

16 128-bit registers, Q0 - Q15
  or
32  64-bit registers, D0 - D32

Interesting instructions:

UXTB (for AES)

VDUP

VEXT (for transpose etc)

VLDx, VSTx (for transposed load?)

VREVx

VSHL (with register)

VSHLL (for rotation, combining with VPADD)

VSLI, VSRI (also an alternative for rotation?)

VTBL (not sure what it can be used for)

VTRN

VUZP, VZIP


For memxor:

veor, can do 4 words.
vext, "extract", but needs separete code for each shift count.
vldr/vldm, plain load
vstr/vstm, plain store
