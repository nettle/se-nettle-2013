function mk_exp_table (S_vals, n_vals)
  c = round (1 + lambertw(0, S_vals * exp(-1)) / log(2));
  for i = 1:length(S_vals)
    printf("%d & %d", S_vals(i), c(i));
    for n = n_vals
	k = round (2^c(i) * n / c(i) / S_vals(i));
	p = ceil(n/k);
	S = 2^c(i) * ceil(p/c(i));
	T = ceil(n/c(i)) + k;
	printf(" & %d, %d, %d", k, S, T);
      endfor
      printf("\\\\\n");
  endfor
endfunction
