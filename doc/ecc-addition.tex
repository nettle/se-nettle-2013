\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{url}

\newcommand{\ceil}[1]{\lceil #1 \rceil}
\newcommand{\FIXME}[1]{\textbf{FIXME: #1}}

\begin{document}

\begin{abstract}
  Notes on methods for ECC addition.
\end{abstract}

\section{Introduction}

We work with elliptic curves of the form
\begin{equation*}
  y^2 = x^3 - 3 x + b \pmod{p}
\end{equation*}
The constant -3 is convenient, and used by standard curves. It saves a
multiplication or two in the duplication formulas.

Operations of interest are
\begin{itemize}
\item Addition of general points.
\item Addition where one point is known in advance. E.g, it can be
  represented in canonical form even if we use homogeneous coordinates
  for other point and for the result.
\item Duplication; adding one point to itself. This case
  \emph{requires} special handling.
\end{itemize}

For simplicity, we'll assume that none of the inputs or outputs is the
unit element. Main reference is Chudnovsky-86.

\section{Affine coordinates}

To add two points, $(x_3, y_3) =  (x_1, y_1) + (x_2, y_2)$, the
equations are
\begin{align}
  m &=
  \begin{cases}
    (y_2 - y_1) / (x_2 - x_1) & (x_1, y_1) \neq (x_2, y_2) \\
    (3 x_1^2 -3) / (2 y_1) & (x_1, y_1) = (x_2, y_2)
  \end{cases} \\
  x_3 &= - (x_1 + x_2) + m^2 \\
  y_3 &= - y_1 + m (x_1 - x_3)
\end{align}
These equations have the drawback that we need a division, i.e.,
inversion modulo $p$.

\section{Plain homogeneous coordinates}

To avoid division, one can use the representation
\begin{align*}
  x &= X / Z & y = Y / Z
\end{align*}
and work with the coordinates $(X, Y, Z)$ instead. 

\subsection{Addition}

To compute $(X_3, Y_3, Z_3) = (X_1, Y_1, Z_1) + (X_2, Y_2, Z_2)$,
first compute some auxillary values.
\begin{align*}
  U_1 &= X_1 Z_2 & S_1 &= Y_1 Z_2 \\
  U_2 &= X_2 Z_1 & S_2 &= Y_2 Z_1 \\
  P &= U_2 - U_1 & R &= S_2 - S_1 \\
\end{align*}
If $P$ or $R$ is zero, it's duplication or cancellation, which must
be handled specially. So assume they're non-zero. The the 
coordinates of the sum are computed as
\begin{align*}
  W &= Z_1 Z_2 \\
  X_3 &= P (- (U_1 + U_2) P^2 + W R^2) \\
  Y_3 &= R (-2W R^2 + 3 (U_1 + U_2)P^2) - P^3 (S_1 + S_2)\\
  Z_3 &= W P^3
\end{align*}
This needs 14 multiplications.

\subsection{Addition of a normalized point}

Now consider the special case that $Z_2 = 1$. Then we can omit three
of the multiplications, for a total of 11 multiplications.

\subsection{Addition of two normalized points}

If both inputs are normalized, and output is in homogeneous
coordinates, we eliminate half of the multiplications, leaving only 7.
\FIXME{Compare to the equations in affine coordinates, putting $Z_3 =
  (x_2 - x_1)^2$}.

\subsection{Duplication}

The formulas for adding two points are different. Compute $(X_2,
Y_2, Z_2) = 2 (X_1, Y_1, Z_1)$ as
\begin{align*}
  W &= 3 (X_1 + Z_1)(X_1 - Z_1) \\
  X_2 &= (2 Z_1 Y_1) \left[W - 4 (2 Z_1 Y_1) X_1 Y_1 \right] \\
  Y_2 &= - Y_1^2 (2Z_1 Y_1)^2 + W (6 (2Z_1Y_1) X_1 Y_1 - W) \\ 
  Y_3 &= (2 Z_1Y_1)^3
\end{align*}
This needs 10 multiplications.

\section{Jacobi coordinates}

Another way to do homogeneous coordinates is the transform
\begin{align*}
  x &= X / Z^2 & y = Y / Z^3
\end{align*}

\subsection{Addition}
Addition first constructs the auxillary variables,
\begin{align*}
  U_1 &= X_1 Z_2^2 & S_1 &= Y_1 Z_2^3 \\
  U_2 &= X_2 Z_1^2 & S_2 &= Y_2 Z_1^3 \\
  P &= U_2 - U_1 & R &= S_2 - S_1 \\
\end{align*}
Again, $P$ and $S$ should be non-zero. Then the cordinates of the sum
are
\begin{align*}
  X_3 &= -(U_1 + U_2)P^2 + R^2 \\
  2 Y_r &= R (-2R^2 + 3 P^2 (U_1 + U_2)) - P^3(S_1 + S_2) \\
  Z_3 &= Z_1 Z_2 P
\end{align*}
This is a total of 16 multiplications.

\subsection{Addition of a normalized point}
Assume that $Z_2 = 1$. Then 5 multiplications disappear, and we are
left with only 11. Bernstein suggests
\begin{align*}
  U_2 &= x_2 z_1^2 \\
  S_2 &= y_2 z_1^3 \\
  H &= U_2 - x_1 \\
  I &= 4 H^2 \\
  J &= I H = 4H^3 \\
  r &= 2 (S_2 - y_1) \\
  V &= x_1 I \\
  x_3 &= r^2 - J - 2 V \\
  y_3 &= r (V - x_3) - 2 y_1 J \\
  z_3 &= (z_1+H)^2 - z_1^2 - H^2
\end{align*}


\subsection{Duplication}

In this case, we compute
\begin{align*}
  M &= 3 (x_1 - z_1^2)(x_1 + z_1^2) \\
  S &= 4 x_1 y_1^2 \\
  x_2 &= -2S + M^2 \\
  y_2 &= - 8 y_1^4 + M (S - x_2) \\
  z_2 &= 2 y_1 z_1
\end{align*}
A total of 8 multiplications. Bernstein suggests (with 5 squarings and
3 muls):
\begin{align*}
  \delta &=z_1^2 \\
  \gamma &= y_1^2 \\
  \beta &= x_1*\gamma \\
  \alpha &= 3*(x_1-\delta)(x_1+\delta) \\
  x_2 &= \alpha^2-8 \beta \\
  z_2 &= (y_1+z_1)^2-\gamma-\delta \\
  y_2 &= \alpha (4 \beta-x_3)-8\gamma^2
\end{align*}
Note that with these formulas, $Z_1 = 0$ implies $z_3 = 0$, so it can
handle the zero point.

If we have $z_1 = 1$, Bernstein suggests (5 squarings, 1 mul)
\begin{align*}
  S &= 2*((x_1+y_1^2)^2-x_1^2-y_1^4) \\
  M &= 3 (x_1^2-1) \\
  x_3 &= M^2-2 S \\
  y_3 &= M (S-x_3)-8 y_1^4 \\
  z_3 &= 2 y_1
\end{align*}


\section{Summary}

\begin{tabular}{r|rr}
  Operation & Homogeneous & Jacobi \\
  \hline
  Add & 14 & 16\\
  Add norm. & 11 & 11\\
  Dup & 10 & 8
\end{tabular}

So if we use a multiplication algorithm donsisting of dup operations
and adds where one of the points is a normalized constant, then Jacobi
coordinates seem to be the best choice.

\section{Montgomery representation}

For some fields, in particular $Z_p$ with $p = 2^{256} - 2^{224} +
2^{192} + 2^{96} - 1$, it is advantageous to use Montgomery
representation. (I think this is unrelated to Montgomery
representation of curve points, which I'm not familiar with). The idea
is that we set $B$ to a suitable power of two, e.g., $B = 2^{256}$,
and define $x'$ = $B x \bmod p$. Then we can multiply as
\begin{equation*}
  (xy)' = B xy = x' y' / B \pmod{p}
\end{equation*}
This is useful if division by $B$ modulo $p$ (often called
\emph{redc}) is more efficient than a regular modulo $p$ operation.

To convert affine coordinates $(x,y)$ to Jacobi coordinates in
Montgomery form, we put
\begin{align*}
  X' &= B x \bmod p & Y' &= B y \bmod p & Z' = B \bmod p
\end{align*}
For the reverse transformation, first compute $v = (Z' / B)^{-1}$. We
then have
\begin{align*}
  x &= X' v^2 / B & y &= Y' v^3 / B
\end{align*}
This requires a couple of modulo operations. To avoid those, an
alternative is to compute $v' = (Z'/B^2)^{-1}$, then
\begin{align*}
  x &= X' v'^2 / B^3 & y &= Y' v'^3 / B^4
\end{align*}

\subsection*{Eliminating mod operations.}

The below idea is maybe not terribly useful, but anyway\ldots Instead
of choosing $Z=1$ when converting from affine to jacobian coordinates,
we could prefer $Z = 1/B$, so that $Z' = 1$.

We now have three different representations of interest:
\begin{enumerate}
\item Standard affine coordinates: $(x, y)$.
\item Jacobian coordinates, with Montgomery representation: $(X', Y',
  Z')$.
\item Normalized Jacobi coordinates, with Montgomery representation
  $(X', Y', 1)$.
\end{enumerate}

To convert from standard affine coordinates $(x, y)$ to normalized
coordinates $(X', Y', 1)$, we use
\begin{align*}
  X' &= x / B \pmod p & Y' &= y / B^2 \pmod p
\end{align*}
These are three redc operations. This eliminates the only(?) remaining
mod operations.

To convert from Jacobi coordinates to normalized coordinates, compute
the inverse $v = Z^{-1} = (Z'/B)^{-1}$ modulo $p$, and $(X', Y', Z')$ is
equivalent to
\begin{equation*}
  (X' v^2 / B^2, Y' v^3 / B^3, 1)
\end{equation*}

To convert normalized $(X', Y', 1)$ coordinates to standard
coordinates, we need to multiply by $B$,
\begin{align*}
  x &= X' B \bmod p & y &= Y' B^2 \bmod p
\end{align*}
 
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
