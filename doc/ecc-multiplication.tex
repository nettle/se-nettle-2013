\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{url}

\newcommand{\ceil}[1]{\lceil #1 \rceil}
\newcommand{\FIXME}[1]{\textbf{FIXME: #1}}

\begin{document}

\begin{abstract}
  Notes on methods for ECC multiplication (aka exponentiation algorithms)
\end{abstract}

\section{Comb method}

The ``comb'' method is described in Handbook of Applied Cryptography.
It's attributed to Lim and Lee, who have a patent on this or some
related algorithm. According to djb, the patent is bogus.

\section{Pippenger's algorithm}

This general and somewhat complicated algorithm from 1976 is described
in a paper by djb. The $L=1$ special case is more or less the same as
the comb method.


\subsection{Special case $L=1$}

I'm trying to decipher djb's description. I will use additive notation
for the group, writing $e x$, where $e$ is a scalar and $x$ is a group
element, rather than $x^e$. Let the bit size of
scalars (or exponents) be $n$ ($\ceil{\lg B}$ in djb's notation). There are two
phases, one precomputation which is independent of the exponent, and
one computation which applies the computed tables to a particular
exponent. 

First the precomputation. Choose an integer $k \geq 1$, and set $p =
\ceil{n / k}$. Compute
\begin{equation*}
  x_i = 2^{ik} x,
\end{equation*}
for $i = 0, 1, \ldots, p-1$, using $(p-1)k \approx n$ doublings.

Choose an integer $c \geq 1$, and let $0 \leq \ell < 2^c$. Write $\ell$
in binary, $\ell = \ell_0 + 2 \ell_1 + \cdots + 2^{c-1} \ell_{c-1}$.
Define
\begin{equation*}
  y_{\ell} = \sum_{i=0}^{c-1} \ell_i x_i = \sum_{i=0}^{c-1} \ell_i
  2^{ik} x
\end{equation*}
For $j = 0, 1, \ldots, \ceil{p/c} - 1$, also precompute
\begin{equation*}
  2^{jkc} y_{\ell}
\end{equation*}
and tabulate these values for all $j$ and $\ell$, for a total of $2^c
\ceil{p/c}$ elements.

Now, consider a particular scalar $e$. First write $e$ in binary, $e =
e_0 + 2 e_1 + \cdots 2^{n-1} e_{n-1}$. Arrange the bits in a grid, and
for simplicity, zero-pad it to $p k$ bits.
\begin{equation*}
  \begin{matrix}
    e_0 & 2 e_1 & 2^2 e_2 & \cdots & 2^{k-1} e_{k-1} \\
    2^k e_k & 2^{k+1} e_{k+1} &2^{k+2} e_{k+2} & \cdots & +
    2^{2k-1}e_{2k-1} \\
    \vdots & \vdots & \vdots\ & \ddots & \vdots \\
    2^{(p-1)k} e_{(p-1)k} & 2^{(p-1)k+1} e_{(p-1)k+1} 
    & 2^{(p-1)k+2} e_{(p-1)k+2} & \cdots & 2^{pk - 1} e_{pk-1}
  \end{matrix}
\end{equation*}
Column $i$ corresponds to the number
\begin{equation*}
  e_i + 2^{i+k} e_{i+k} + 2^{i+2k} e_{i+2k} + \cdots + 2^{i + (p-1) k} e_{i+(p-1)k}
\end{equation*}
Rearrange these bits, splitting them into $M = \ceil{p/c}$ pieces of
$c$ bits, indexed by $j = 0, 1, \ldots, M-1$:
\begin{equation*}
  e_{i,j} = e_{i + ckj} + 2 e_{i + (cj+1) k} + 2^2 e_{i + (cj+2) k} +
  \cdots 2^{c-1} e_{i + (cj+c-1)k}
\end{equation*}
The product corresponding to the bits included in $e_{i,j}$ is
\begin{multline*}
  (2^{i + kcj} e_{i+ckj} + 2^{i + (cj+1) k} e_{i + (cj+1) k} + \cdots 
  2^{i + (cj+c-1)k} e_{i + (cj+c-1)k}) x \\
  = 2^{cjk + i} (e_{i+ckj} + 2^k e_{i + (cj+1) k} + \cdots 
  2^{(c-1)k} e_{i + (cj+c-1)k}) x \\
  = 2^{ckj + i}y_{e_{i,j}}
\end{multline*}
So we can compute
\begin{equation*}
  e x = \Big(\cdots \big( \sum_j 2^{ckj}y_{e_{k-1,j}} \cdot 2 + \sum_j
  2^{ckj}y_{e_{k-2,j}}\big)\cdot 2 +
  \cdots\Big)\cdot 2 + \sum_j 2^{ckj}y_{e_{0,j}}
\end{equation*}
The partial $\sum_j$ expressions consist of a total of $k(M-1)$
additions, and we then have an additional $k-1$ doublings and adds,
for a total of $kM-1 \approx n/c$ adds and $k-1$ doublings.

\subsection{No adds are doublings}

For application to ECC, addition $x + y$ usually can't handle the
special case $x = \pm y$ (doubling and cancellation) without special
code, and triggering special code for some inputs can leak information
via side channels such as timign and caches. Also the zero element
needs to be handled specially.

If we assume that $e$ is smaller than the group order, we only have
zero elements to worry about, but no doubling and cancellations.
Cancellations clearly can't happen since all additions are of the form
$e_1 x + e_2 x$ where all of $e_1$, $e_2$ and $e_1+e_2$ are smaller
than the order of $x$. Also, $2^{ckj} y_{i,j} \neq 2^{ckj'} y_{i,j'}$
when $j \neq j'$ and $i > 0$ (when $i = 0$, both are zero).

Furthermore, the chain of doubling and add adds elements $e_1 x + e_2
x$ where the $e_1$ and $e_2$ scalars always have their one bits in
disjoint positions, hence $e_1 \neq e_2$ whenever they're non-zero.

\subsection{Choice of parameters}

Precomputation and storage is $S = 2^c n / (kc)$, time for the
exponentiation is $T = n/c + \alpha k$, where $\alpha$ is the cost of
a squaring in terms of the cost of a multiplication. Assume we have
fixed storage size $S$, what are the optimal parameters? We can first
solve for $k$ in terms of $c$,
\begin{equation*}
  k = \frac{2^c n}{cS}
\end{equation*}
Substituting into the running time, excluding the precomputation, we
get
\begin{equation*}
  \frac{n}{c} + \alpha \frac{2^c n}{cS}
\end{equation*}
Let $f(c)$ be this value times $S/n$:
\begin{equation*}
  f(c) = \frac{S + \alpha 2^c}{c}
\end{equation*}
To find the minimum, differentiate,
\begin{equation*}
  f'(c) = \frac{\alpha c 2^c \log 2 - (S + \alpha 2^c)}{c^2}
\end{equation*}
Setting this to zero, we get the equation
\begin{equation*}
  (c \log 2 - 1) 2^c = \frac{S}{\alpha}
\end{equation*}
Rearrange this as
\begin{equation*}
  (c \log 2 - 1) \exp(c \log 2 - 1) = \frac{S \exp (-1)}{\alpha}
\end{equation*}
The solution can then be expressed using Lambert's function,
\begin{equation*}
  c = \frac{1}{\log 2} \left(1 + W_0 (S \exp (-1) / \alpha)\right)
\end{equation*}
For large $S$, we can use the approximation $W_0(x) \approx \log(x) -
\log \log (x)$, to get
\begin{equation*}
  c \approx \log S / \log(2) - \log \log S - 0.45
\end{equation*}

For comparison with Bodo Möller, consider the case of $n = 160$.
According to \url{http://www.bmoeller.de/pdf/fastexp-icisc2002.pdf},
window NAF splitting needs 160 elements, 7.2 squarings and
26.7 multiplies (total 33.9), while the ``Lim Lee''-algorithm can use
128, 13 squarings and 26.6 multiplies (total 39.6) or 256 elements, 11
squarings and 22.8 multiplies (total 33.2).

With the above special case of Pippinger's algorithm, $c = 6$ seems
optimal for $n = 160$. This gives us 27 multiplies, and a range where
$k = 14$ needs 122 elements and 13 squarings (total 40), down to $k =
7$ which gives us 244 elements and 6 squarings (total 33).

\pagebreak

For larger sizes, the following table gives close-to-optimal
parameters, tabulated by $n$ and $S$.

\centerline{\begin{tabular}{rr|rrrrr}
  $n$ && 256 & 512 & 1024 & 2048 & 4096 \\
  Appr. $S$ & $c$ &  $k, S, T$ \\
  \hline
   16 &  2 & 41, 22, 130 & 82, 22, 260 & 164, 22, 519 & 328, 22, 1038 & 656, 22, 2076\\
   32 &  4 & 37, 40, 97 & 74, 40, 193 & 149, 40, 386 & 297, 40, 771 & 594, 40, 1541\\
   64 &  4 & 19, 80, 79 & 37, 80, 156 & 74, 80, 311 & 149, 80, 623 & 297, 80, 1244\\
  128 &  5 & 19, 163, 64 & 38, 163, 127 & 76, 163, 254 & 151, 163, 506 & 303, 163, 1013\\
  256 &  5 & 9, 327, 54 & 19, 272, 108 & 38, 272, 216 & 76, 272, 431 & 151, 272, 861\\
  512 &  7 & 10, 593, 46 & 21, 593, 92 & 41, 593, 183 & 82, 593, 366 & 165, 593, 733\\
 1024 &  7 & 5, 1187, 41 & 10, 1187, 81 & 21, 1038, 163 & 41, 1038, 325 & 82, 1038, 650\\
 2048 &  8 & 6, 2017, 36 & 12, 2017, 72 & 23, 2420, 142 & 47, 2420, 284 & 93, 2420, 567\\
 4096 & 10 & 7, 4386, 33 & 14, 4386, 65 & 27, 4386, 129 & 54, 4386, 257 & 109, 4386, 515\\
 8192 & 10 & 3, 9869, 29 & 7, 8773, 58 & 14, 8773, 116 & 27, 8773, 230 & 54, 8773, 460\\
16384 & 11 & 4, 17885, 27 & 8, 17885, 53 & 16, 17885, 105 & 32, 17885, 210 & 65, 17885, 420\\
32768 & 11 & 2, 35771, 25 & 4, 35771, 49 & 8, 35771, 97 & 16, 35771, 194 & 32, 35771, 387\\
65536 & 12 & 2, 81030, 22 & 5, 64824, 45 & 10, 64824, 89 & 20, 64824, 178 & 39, 72927, 355\\
\end{tabular}}

To compare with basic binary, and basic comb:

\centerline{\begin{tabular}{rr|rrrrr}
  $n$ && 256 & 512 & 1024 & 2048 & 4096 \\
  $S$ &$k$ $T$ \\
  \hline
    0 &    & 384 & 768 & 1536 & 3072 & 6144 \\
  \hline
   16 &  4 & 128 & 256 &  512 & 1024 & 2048 \\
   32 &  5 & 103 & 205 &  410 &  820 & 1639 \\
   64 &  6 &  86 & 171 &  342 &  683 & 1366 \\
  128 &  7 &  74 & 147 &  293 &  586 & 1171 \\
  256 &  8 &  64 & 128 &  256 &  512 & 1024 \\
  512 &  9 &  57 & 114 &  228 &  456 &  911 \\
 1024 & 10 &  52 & 103 &  205 &  410 &  820 \\
 2048 & 11 &  47 &  94 &  187 &  373 &  745 \\
 4096 & 12 &  43 &  86 &  171 &  342 &  683 \\
 8192 & 13 &  40 &  79 &  158 &  316 &  631 \\
16384 & 14 &  37 &  74 &  147 &  293 &  586 \\
32768 & 15 &  35 &  69 &  137 &  274 &  547 \\
65536 & 16 &  32 &  64 &  128 &  256 &  512 \\
\end{tabular}}

\end{document}

