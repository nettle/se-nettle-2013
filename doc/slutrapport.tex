\documentclass[a4paper,swedish]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{babel}
\usepackage{url}
\usepackage{xspace}
\usepackage{amsmath}
\usepackage{array}
\usepackage{dcolumn}

\author{Niels Möller, Southpole AB}
\title{Anpassning av GNU Nettle för inbyggda system}
\date{April 2013}

\hyphenation{ar-ki-tek-tu-ren min-nes-åt-gång}

\makeatletter
\newcommand{\defineabbrev}[1]{\@namedef{#1}{\textsc{\lowercase{#1}}}}
\newcommand{\defineabbrevx}[1]{\@namedef{#1}{\textsc{\lowercase{#1}}\xspace}}
\makeatother

\defineabbrevx{ARM}
\defineabbrevx{AES}
\defineabbrevx{ECC}
\defineabbrev{SHA}
\defineabbrevx{DES}
\defineabbrevx{RSA}
\defineabbrevx{DSA}
\defineabbrevx{GNU}
\defineabbrevx{GMP}
\defineabbrevx{RFC}
\defineabbrev{SSE}
\defineabbrevx{XOR}
\defineabbrevx{LGPL}
\defineabbrevx{NIST}
\defineabbrevx{HMAC}
\defineabbrevx{UMAC}
\defineabbrevx{ECDSA}

\newcolumntype{d}[1]{D{.}{.}{#1}}

\begin{document}

\maketitle
%%\begin{abstract}
%%  Projektet ''Anpassning av \GNU Nettle för inbyggda system'' har
%%  pågått under våren 2013, med finansiering från Internetfonden.
%%\end{abstract}

\tableofcontents

\clearpage

\section{Inledning}

Projektet ''Anpassning av \GNU Nettle för inbyggda system'' har
pågått under våren 2013, med finansiering från Internetfonden.

\GNU Nettle är ett minimalistiskt kryptobibliotek som tillhandahåller
kryptografiska byggstenar: hashfunktioner (som MD5, \SHA1, och \SHA256),
krypterinsalgoritmer (som \DES, \AES, Camellia, Salsa20) och digitala
signaturer (som \RSA och \DSA). Nettle är licensierat under \GNU Lesser
General Pubic License (\LGPL), och releaser är tillgängliga på
\url{ftp://ftp.gnu.org/gnu/nettle}.

Inbäddade och mobila enheter, ofta baserade på
\ARM-arkitekturen, blir allt vanligare. Samtidigt är storskalig
avlyssning av Internetkommunikation på frammarsch. Effektiv kryptering
även på mobila enheter är därför avgörande för att skydda privat
kommunikation.

Många inbäddade och mobila enheter, bland annat smartphones och
hemmaroutrar, använder processorer ur \ARM-familjen. \GNU Nettle har
fram tills det här projektet saknat optimeringar specifikt för dessa
processorer.

\section{Mål och syfte}

Projektets övergripande syfte är att göra kryptering med hjälp av
biblioteket \GNU Nettle mer praktiskt och effektivt för inbyggda system
och mobila enheter. De två konkreta delmålen för projektet har varit:
\begin{itemize}
\item Att implementera digitala signaturer baserade på elliptiska
  kurvor (\ECC). Jämfört med traditionella digitala signaturer med
  \RSA, så ger elliptiska kurvor motsvarande säkerhet med betydligt
  mindre processortid. Även minnesåtgång, både för själva beräkningen,
  och för nycklar och signaturer, är betydligt mindre.
\item Att optimera andra viktiga kryptografiska byggstenar, som
  krypteringsalgoritmen \AES och hashfunktionerna \SHA1 och \SHA256, för
  \ARM-ar\-ki\-tek\-tu\-ren.
\end{itemize}

\section{Projektbeskrivning}

\subsection{Utvecklingssystem}

För \ARM-utveckling, har jag använt en Pandaboard med en \ARM
Cortex-A9 processor, \url{http://www.pandaboard.org/content/platform}.
Som operativsystem har jag använt Debian \GNU/Linux.

\subsection{Elliptiska kurvor}

För kryptografiska ändamål är en elliptisk kurva en matematisk grupp
av ''punkter på kurvan''. För de kurvor som jag har arbetat med, så
definieras kurvan av ekvationen
\begin{equation*}
  y^2 = x^3 - 3 x + b \pmod p.
\end{equation*}
Koordinaterna $x$ och $y$ ska alltså räknas modulo ett primtal $p$,
och ''punkterna på kurvan'' är helt enkelt de par av $(x, y)$ som
uppfyller ekvationen. Kurvan definieras av primtalet $p$ och
konstanten $b$. Det som gör elliptiska kurvor intressanta för
kryptografiska tillämpningar är att man kan konstruera en sorts
''addition'' som givet två punkter på kurvan ger en ny punkt. Om man
dessutom kompletterar med en oändlighetspunkt, så får man något som
uppfyller den abstrakta algebrans axiom för en ''grupp''.

\subsection{Designval för elliptiska kurvor}

\subsubsection{Val av kurvor}

Det första beslutet gällde vilka kurvor som ska stödjas. Den
vanligaste algoritmen för digitala signaturer baserade på elliptiska
kurvor är \ECDSA. I \NIST{}s specifikation,
\url{http://csrc.nist.gov/publications/fips/fips186-3/fips_186-3.pdf},
rekommenderar fem kurvor, med olika säkerhetsnivå. Motsvarande primtal
$p$ är valda för att reduktion modulo $p$ ska kunna göras enkelt med
ett fåtal skift och additioner. Samtliga dessa kurvor har
implementerats. \NIST rekommenderar också fem kurvor av en annan typ,
där aritmetik på koordinaterna inte görs med vanlig modulo; dessa har
inte implementerats.

\subsubsection{Sidokanaler}

En ''sidokanalsattack'' på ett kryptosystem är en attack som utnyttjar
annan information än de överförda meddelandena. Till exempel
observation av tidsåtgång och effektförbrukning för den maskin som
utför krypteringen med en hemlig nyckel. En annan typ av attack kan
bli möjlig om man har flera processer som kör på samma maskin, och
delar processorns cacheminne. Genom att manipulera cachen och mäta hur
lång tid olika minnesaccesser tar, kan en process få information om
vilka minnesadresser en annan process läser. Och om den andra
processen till exempel använder delar av en hemlig nyckel som index i en
tabell, så kan den här typen av attacker läcka information om den
hemlig anyckeln.

Både attacker baserade på mätning av tidsåtgång, och attacker via
processorns cache, går att skydda sig mot genom att skriva de rutiner
som hanterar hemligt data så att de exekverar precis samma
instruktionssekvens och läser precis samma minnesadresser, oavsett
innehållet i det hemliga datat. Man behöver undvika alla villkorliga
hopp som beror på innehåll, och en enkel tabelluppslagnign måste
ersättas med kod som läser \emph{hela} tabellen från början till slut,
och använder logiska operationer för att extrahera rätt element.

Timing-attacker är relevanta för de flesta system som kommunicerar
över internet, och i synnerhet såna som är ganska långsamma.
Cache-baserade attacker kan vara relevanta för till exempel
smartphones, där många applikationer av olika grad av pålitlighet körs
på samma enhet. Jag bestämde mig därför för att sikta på att
implementera elliptiska kurvor på ett sådant sätt att den inte ska
läcka information via timing eller cache. Effektförbrukning har jag
däremot inte tittat på. Att skydda sig mot läckor den vägen kräver
troligen intim kunskap om aktuell hårdvara.

\subsubsection{Koordinatrepresentation}

Addition av punkter kan skrivas på lite olika sätt, baserat på
modulo-$p$-aritmetik på koordinaterna. Det enklaste sättet kräver även
en division modulo $p$, vilket är en mycket dyrare operation än
multiplikation och addition. Man kan eliminera nästan alla divisioner
genom att införa en extra koordinat; en sammanställning av olika
varianter finns på
\url{http://www.hyperelliptic.org/EFD/g1p/auto-shortw.html}.

Nästa designval gällde representation av koordinaterna. Ett effektivt
och förhållandevis enkelt sätt är Jacobi-koordinater. Istället för att
räkna med de två koordinaterna $(x, y)$ inför man tre koordinater
$(X,Y,Z)$, som hänger ihop med de ursprungliga koordinaterna genom
variabelbytet
\begin{align*}
  x &= X/Z^2 & y &= Y / Z^3.
\end{align*}
Poängen med den redundanta koordinaten $Z$ är att när man med de
ursprungliga koordinaterna skulle behöva dividera, så kan man i
stället multiplicera $Z$ med motsvarande tal.\footnote{Det enklare
  ''projektiva'' variabelbytet $x = X/Z$, $y = Y/Z$ ger också denna
  fördel, men Jacobi-kordinater blir ännu lite effektivare. En ännu
  effektivare representation är ''Edwards-koordinater'', men tyvärr
  fungerar de inte för de standardkurvor som rekommenderas för
  \ECDSA.}

\subsubsection{Algoritmer för punkt-multiplikation}

För kryptografiska ändamål räcker det inte att kunna addera är den
centrala operationen inte bara addition, utan även
''punkt-multiplikation'' som tar ett heltal $k$ och en punkt $P$ och
räknar ut en ny punkt $k P$ genom att i princip addera $P$ till sig
självt upprepade gånger. Baserat på addition och dubblering ($2P = P +
P)$ kan detta göras på många olika sätt. För att till exempel
multiplicera en punkt $P$ med talet 7, är några alternativ
\begin{align*}
  7 P &= P + P + P + P + P + P + P & \text{6 additioner} \\
  7 P &= 2(2P+P)+P & \text{2 dubbleringar, 2 additioner} \\
  7 P &= 2(2(2P)) - P & \text{3 dubbleringar, en subtraktion}
\end{align*}
Punkt-multiplikation är användbar för kryptografi för att det är en
''envägsfunktion'': Det är förhållandevis enkelt att räkna ut $k P$
för vilket heltal $k$ och vilken punkt $P$ som helst. Men om man bara
får punkten $P$ och resultatet av multiplikationen, punkten $k P$, så
är det i praktiken omöjligt att räkna ut $k$.

Ett nyckelpar i \ECDSA består av en hemlig nyckel, ett heltal $k$, och
en publik nyckel som är punkten $V = k G$, där $G$ är generatorn för
den elliptiska kurva man valt att använda. Säkerhetsnivån för en
elliptisk kurva är kvantitativt sätt att beskriva hur svårt det är att
givet bara den publika nyckeln knäcka systemet genom att räkna ut
motsvarande hemliga nyckel.

För \ECDSA finns det två fall av multiplikation, som kan optimeras på
olika sätt. För att skapa en signatur, behöver man multiplicera en fix
punkt, gruppens generator, med ett slumpmässigt tal. Här kan man vinna
mycket tid på att förberäkna tabeller med väl valda multipler av
generatorn. Man behöver en tabell per kurva, men sen kan man använda
tabellen för alla signaturer man vill göra baserat på den kurvan.

För att verifiera en signatur, behöver man göra två
punkt-multiplikationer, den ena med generatorn, och den andra med den
publika nyckeln som ju är olika för varje användare. Det här gör att
det är en betydligt dyrare operation att verifiera än \ECDSA-signatur
än att skapa den.

Så åter till algoritmer för punkt-multiplikation. En bra översikt finns
på \url{http://cr.yp.to/papers/pippenger.pdf}. För punkt-multiplikation
med gruppens fixa generator, som alltså är arbetshästen för skapande
av \ECDSA-signaturer, har jag valt att använda ett specialfall av
Pippengers algoritm från 1976. Algoritmen beskrivs kortfattat i det
ovan nämnda papperet, och tas även upp i Handbook of Applied
Cryptography, avsnitt 14.6.3, under namnet ''Comb method'', se
\url{http://cacr.uwaterloo.ca/hac/about/chap14.pdf}. Algoritmen har
senare återupptäckts och till och med patenteras (US Patent 5999627,
som rimligen är ogiltigt). 

Storleken på de förberäknade tabellerna är en trade-off mellan minne
och processortid. Jag har valt att använda måttligt stora tabeller, ca
16 KByte vardera för de fem kurvor som stöds.

För punktmultiplikation med en allmän punkt, en operation som behövs
för att verifiera \ECDSA-signaturer, använder jag en rätt basal
fönsterbaserad metod som först räknar ut en liten tabell, vid runtime,
och sedan slår upp 4~bitar i taget ur talet man ska multiplicera med.

Om storleken på talet man vill multiplicera med är $n$-bitar, så kan
man genom att arbeta på en bit i taget multiplicera med hjälp av $n$
additioner och $n$ dubbleringar. I grunden bygger även båda de
implementerade algoritmerna på upprepade dubbleringar och additioner.
Den fönsterbaserade metoden använder tabelluppslagning för att
reducera antalet additioner, men den kräver fortfarande lika många
dubbleringar. Poängen med förberäkningen i Pippengers algoritm är att
den reducerar \emph{både} antalet dubbleringar och antal additioner.

\subsection{ARM-optimeringar}

För många av de operationer som görs i Nettle, så kan man vinna en hel
del prestanda på att implementera de mest kritiska funktionerna i
assembler. Det kan handla om 10\%-30\% uppsnabbning tack vara att
övergripande förståelse för algoritmen kan ge bättre
registerallokering än C-kompilatorn klarar av. I vissa fall, när det
finns vinster i att använda instruktioner som C-kompilatorn inte
känner till, så kan assemblerimplementation ge mer dramatiska
uppsnabbningar, uppemot fem gånger.

\subsubsection{\ECC-operationer}

För implementationen av elliptiska kurvor, så används biblioteket \GNU
\GMP för operationer på koordinaterna, och \GMP inkluderar optimerad
assemblerkod för de mest kritiska funktionerna för många olika
plattformar. Men det är ändå ett par funktioner som varit aktuella för
assemblerimplementation, och det är kod för reduktion modulo $p$.
Varje gång \ECC-koden behöver multiplicera två koordinater, räknar den
först ut en vanlig produkt med hjälp av \GMP:s funktioner. Men sen
behöver produkten reduceras modulo $p$.

För reduktion modulo $p$ vill man utnyttja den struktur som finns i
$p$. För att ta ett konkret exempel, så använder den minsta kurvan
primtalet $p = 2^{192} - 2^{64} - 1$. Då kan vi göra reduktion genom
''vikning'', 128 bitar i taget. Låt $x$ vara ett tal på 320 bitar,
vilket på en 32-bits arkitektur som \ARM representeras som en array av
10 stycken 32-bits tal $x_i$. Genom att utnyttja att $2^{192} = 2^{64}
+ 1 \pmod p$, kan vi vika enligt schemat i Tabell~\ref{tab:modp-192}.
För att reducera en produkt på 384 bitar till 192 behöver man vika på
det här sättet två gånger.

\begin{table}
  \centering
  \begin{tabular}{cccccccccccc}
    Vikt & & $2^{9 \cdot 32}$ & $2^{8 \cdot 32}$ & $2^{7 \cdot 32}$ & $2^{6 \cdot 32}$& $2^{5 \cdot 32}$ & $2^{4 \cdot 32}$& $2^{3 \cdot 32}$ & $2^{2 \cdot 32}$& $2^{32}$ & $1$ \\
    \hline
    $x$ & $=$ & $x_9$ & $x_8$ & $x_7$ & $x_6$ & $x_5$ & $x_4$ & $x_3$ & $x_2$ & $x_1$ & $x_0$ \\
    & $=$ & & & & & $x_5$ & $x_4$ & $x_3$ & $x_2$ & $x_1$ & $x_0$ \\
    & $+$ & & & & & $x_9$ & $x_8$ & $x_7$ & $x_6$ & & \\
    & $+$ & & & & & & & $x_9$ & $x_8$ & $x_7$ & $x_6$ \\
  \end{tabular}  
  \caption{Reduktion modulo $p = 2^{192} - 2^{64} - 1$.}
  \label{tab:modp-192}
\end{table}

Den här strukturen går förstås att utnyttja även för implementation i
C, men det blir en del overhead efter som det blir många anrop till
\GMP, var och en för rätt små operationer. En assemblerimplementation
ger mindre overhead. Dessutom kan man utnyttja att talen är så pass
små att de ryms i tillgängliga register, och man kan utnyttja
add-with-carry-instruktionen till fullo. Implementationen i
\ARM-assembler består av 28 additionsinstruktioner samt några load och
store, och ger drygt tre gångers uppsnabbning jämfört med
implementationen med C och \GMP-anrop.

\subsubsection{Andra kryptoprimitiver}

Andra funktioner som har optimerats för \ARM är \texttt{memxor}, \AES,
Salsa20, \SHA1, \SHA256, \SHA512 är \SHA3. \ARM-arkitekturen har 16
stycken generella register om 32 bitar. Vissa processorer, bland andra
Cortex-A9, implementerar också en utökning med vektorinstruktioner,
kallad ''Neon''. Med Neon får man 16 stycken 128-bitars register, som
kan representera vektorer av flyttal eller heltal av storlek upp till
64~bitar. Neon instruktioner ger en dramatisk uppsnabbning för de
algoritmer som behöver 64-bitars operationer, som hashfunktionerna
\SHA512 och \SHA3. Även krypteringsalgoritmen Salsa20 vinner en del på
användning av Neon-instruktioner som hanterar vektorer av fyra stycken
32-bitars tal.

\subsection{Övrigt}

Under projektets gång har det varit naturligt att även göra en del
optimeringar för Intel-arkitekturen x86\_64. Dels för \ECC-primtiver,
dels \SHA256 och \SHA512 som det inte fanns assemblerkod för sedan
tidigare.

Jag har även implementerat \UMAC, en förhållandevis ny algoritm för
autenticering av meddelanden med en delad nyckel. \UMAC beskrivs i
\RFC 4418, och algoritmen är designad för hög prestanda på moderna
datorarkitekturer med effektiv heltalsmultiplikation. Implementationen
har optimerats för både \ARM och x86\_64, och den är drygt 10 gånger
snabbare än alternativ som \HMAC-SHA256 och \HMAC-\SHA512.

\section{Leverabler}

Det viktigaste leverabeln är releasen av nettle-2.7, som inkluderar
den kod som utvecklats under projektet, samt dokumentation av nya
funktioner. Manualen finns även online på
\url{http://www.lysator.liu.se/~nisse/nettle/nettle.html}. Utöver
denna slutrapport har tre statusrapporter skickats till
Internetfonden och till mailinglistan, se
\url{http://lists.lysator.liu.se/pipermail/nettle-bugs/2013/002591.html},
\url{http://lists.lysator.liu.se/pipermail/nettle-bugs/2013/002615.html},
\url{http://lists.lysator.liu.se/pipermail/nettle-bugs/2013/002667.html}.

\section{Resultat}

\subsection{\ECDSA-signaturer}

Implementationen av av digitala signaturer med \ECDSA och elliptiska
kurvor stödjer fem standardkurvor, ''secp192r1'', ''secp224r1'',
''secp256r1'', ''secp384r1'' and ''secp521r1'', där secp256r1 troligen
är den som är mest relevant för tillämpningar. Jag mäter prestanda i
antal signaturer och verifieringar per sekund. Se
Tabell~\ref{tab:ecc-benchmark}.
\begin{table}[t]
  \centering
  \begin{tabular}{lrrrrr}
    & & \multicolumn{2}{c}{Nettle} & \multicolumn{2}{c}{OpenSSL} \\
    Algoritm & Bitstorlek & \multicolumn{1}{r}{Sign./s} & \multicolumn{1}{r}{Ver./s} & \multicolumn{1}{r}{Sign./s} & \multicolumn{1}{r}{Ver./s} \\
    \hline
    \RSA  & 1024 &  495 & 9140 \\
    \RSA  & 2048 &   83 & 2683 \\
    \DSA  & 1024 &  982 &  500 \\
    \ECDSA & 192 & 1464 &  553 \\
    \ECDSA & 224 & 1039 &  411 & 184 & 156 \\
    \ECDSA & 256 &  806 &  319 \\
    \ECDSA & 384 &  343 &  136 & 70 & 59 \\
    \ECDSA & 521 &  184 &   74 & 26 & 21 \\
  \end{tabular}
  \caption{Prestanda för digitala signaturer med \RSA, \DSA och
    \ECDSA, mätt som antal signaturer och antal verifieringar per
    sekund. Mätt på en 1~GHz \ARM Cortex-A9.}
  \label{tab:ecc-benchmark}
\end{table}

Om vi jämför 2048-bitars \RSA med 224-bitars \ECDSA (som anses ge
samma eller till och med en aning högre säkerhet, se
\url{http://www.keylength.com/en/3/}, så är signering med \ECDSA hela
12.5 gånger snabbare än med \RSA. Med \ECDSA är verifiering av
signaturer en dyrare operation än att skapa en signaturen, omkring 2.5
gånger långsammare. För \RSA är det tvärtom, verifiering är många
gånger snabbare än signering\footnote{Detta gäller om man väljer en
  lite publik exponent för \RSA, i de här mätningarna användes $e =
  65537$.}. Det gör att om man jämför prestanda för verifiering är
\ECDSA är 6.5 gånger långsammare än \RSA (men verifiering av \ECDSA är
fortfarande betydligt snabbare än \emph{signering} med RSA).

Det är också intressant att jämföra prestanda med OpenSSL. Här är
Nettle 5--7 gånger snabbare än OpenSSL för signering med \ECDSA, och
2--3 gånger snabbare för verifiering.

Mätningar på Intel x86\_64 ger liknande resultat, fast med lite mindre
skillnad mot OpenSSL.

\subsection{Sidokanalstysta operationer}

För de flesta rutiner har det varit ganska rättframt att skriva
sidokanalstyst kod, och till en ganska liten prestandaförlust. Det
finns två undantag. 

Det ena är tabelluppslagning, där man för att undvika sidokanaler
måste läsa igenom hela tabellen för att plocka ut ett element. Det
blir ändå begränsad kostnad, tack vare att de största tabeller som
används, för Pippengers algoritm, accessas delvis sekventiellt, och
därmed är det för varje uppslagning bara en deltabell som behöver
läsas igenom helt.

Det andra undantaget är modulär invertering, alltså att givet $a$ och
$p$ bestämma ett $x$ sådant att $a x = 1 \pmod p$. Standardmetoden är
att gå via en algoritm för största gemensamma delare, antingen
Euklides' algoritm eller Steins binära algoritm. Men båda varianterna
innehåller steg som är svåra att skriva utan villkorliga hopp som
beror av indata. Jag har inte hittat någon sidokanalstyst
inverteringsalgoritm i litteraturen, så den algoritm jag använder, en
villkorsfri variant av den binära algoritmen för största gemensamma
delare, är troligen ny. Den är uppemot 50 gånger långsammare än \GMP:s
vanliga rutiner för största gemensamma delare, och tar troligen runt
25\% av tiden för att skapa en \ECDSA-signatur.

\subsection{Optimering av andra kryptoprimitiver}

I Tabell~\ref{tab:nettle-benchmark} visas prestanda för ett urval av
de kryptoprimitiver som implementeras i Nettle. Arcfour och
Triple-\DES är äldre men vanliga krypteringsalgoritmer, \AES och
Salsa20 är två nyare. De olika SHA-funktionerna är kryptografiska
hashfunktioner. \UMAC, som nämndes tidigare, är en hashfunktion med
nyckel som anvädns för autenticering av meddelanden. \XOR
representerar bitvis ''exklusivt eller'' av två minnesblock. Bland
applikationer som är i drift idag är \AES och \SHA1 de två viktigaste
av funktionerna på listan. Framöver kan Salsa20 och \UMAC bli vanliga
för tillämpningar där hög prestanda är viktigt.
\begin{table}[t]
  \centering
  \begin{tabular}{ld{1}d{1}r}
    & \multicolumn{1}{r}{C-kod} & \multicolumn{1}{r}{\ARM-assembler} \\
    Namn & \multicolumn{1}{r}{MByte/s} & \multicolumn{1}{r}{MByte/s} &
    \multicolumn{1}{r}{Skillnad, \%} \\
    \hline
    \AES-128  &   17.3 &   22.1 &  27 \\
    \AES-192  &   14.5 &   18.5 &  28 \\
    \AES-256  &   12.5 &   16.0 &  28 \\
    Arcfour   &   46.8 \\
    Triple-\DES &  4.7 \\
    Salsa20   &   39.8 &   58.2 &  46 \\
    \SHA1     &   55.7 &   60.7 &   9 \\
    \SHA256   &   24.6 &   31.7 &  29 \\
    \SHA512   &    7.8 &   30.4 & 291 \\
    \SHA3-256 &    5.5 &   26.0 & 378 \\
    \SHA3-512 &    2.9 &   13.9 & 380 \\
    \UMAC-32  &  379.6 &  932.1 & 146 \\  
    \UMAC-128 &  149.8 &  349.0 & 133 \\
    \XOR      &  987.3 & 1905.9 &  93 \\
  \end{tabular}
  \caption{Prestanda för några av Nettles kryptografiska algoritmer.
    Mätt på en 1~GHz \ARM Cortex-A9.}
  \label{tab:nettle-benchmark}
\end{table}

För de kanske viktigaste algoritmerna, \SHA1 och \AES, har
optimeringen gett en måttlig uppsnabbning, upp till knappt 30\%. Den
största uppsnabbningen ser vi för \SHA512 och \SHA3, som båda är
beroende av 64-bits operationer. Även för \UMAC blev det en mer än en
fördubbling av prestanda, tack vare användning Neon-instruktioner.
En assemblerrutin för \UMAC i \ARM-assembler \emph{utan} Neon
instruktioner skulle troligen också kunna ge en rätt bra uppsnabbning,
och vore till stor nytta för de \ARM-processorer som saknar Neon.

En erfarenhet är att Neon-instruktionerna är användbara och trevliga
att arbeta med, i synnerhet i jämförelse med det ganska hemska hopkok
av vektorinstruktioner som finns för x86-processorer, med \SSE2 och
relaterade utökningar.

\section{Utvärdering och analys}

\subsection{Utvärdering av resultat}

Den funktionalitet som planerades har blivit klar. Det fanns inga
kvantitativa mål för prestandamen för \ECDSA tycker jag man kan vara
ganska nöjd med att signaturer går en storleksordning snabbare än
\RSA, och att koden dessutom är ett par gånger snabbare än OpenSSL.

På \ARM-sidan har utvecklingen varit begränsad till en plattform, en
Pandaboard med \ARM Cortex-A9. Den Raspberry Pi som också har funnits
tillgänglig har det inte blivit tid att utveckla för.

Projektets kostnader är i huvudsak arbetstid, planerat 420 timmar. Det
har dragit över planen ett par dagar, i skrivande stund har 435 timmar
lagts på projektet, delvis på grund av implementationen av \UMAC som
inte var planerad då projektet startade, men som ändå känns relevant
för inbyggda tillämpningar som behöver hög prestanda på begränsad
hårdvara.

\subsection{Förslag på förbättringar}

\begin{itemize}
\item Design och dokumentation av underliggande \ECC-primitiver, för
  att göra det enkelt att implementera andra saker än just digitala
  signaturer enligt \ECDSA.
\item För vissa \ECC-operationer, i synnerhet verifiering av
  \ECDSA-signaturer, behandlas inga hemliga data. Då gör det inget om
  information om datat läcker genom sidokanaler. Det skulle gå att
  skriva alternativa, lite snabbare, rutiner för denna användning.
\item Konfigurering vid run-time av vilka assembler-rutiner som ska
  användas, beroende på vilken processormodell som programmet körs på.
  För \ARM handlar det främst om att välja om rutiner som använder
  Neon-instruktioner kan användas eller inte. Men en sådan mekanism
  vore till stor nytta även på x86, där vissa processorer har
  särskilda instruktioner för till exempel \AES.
\item Test och optimering för andra processorer i \ARM-familjen.
\item Det finns troligen en del utrymme kvar för optimering av de
  olika assemblerrutinerna, till exempel genom mer genomtänkt
  schemaläggning av instruktionerna.
\end{itemize}

\section{Framtida arbeten}

För nästa release av Nettle planeras en mindre uppstädning av
programmeringsgränssnittet. Större ändringar som kan bli aktuella i
kommande releaser:
\begin{itemize}
\item Konfigurering vid run-time av vilka assembler-rutiner som ska
  användas, som nämndes i föregående avsnitt.
\item Design av gränssnitt för konstruktioner som ger både kryptering
  och autenticering.
\item Stöd för digitala signaturer där den privata nyckeln lagras i
  separat hårdvara, till exempel ett smartcard.
\item Möjlighet att använda mini-gmp (en liten och mindre effektiv
  implementation av en den viktigaste funktionaliten i \GNU \GMP), som
  en fallback ifall \GNU \GMP av någon anledning inte är
  tillgängligt.
\end{itemize}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
