Including memxor assembly:

$ examples/nettle-benchmark -f 1e9

benchmark call overhead: 0.008942 us   8.94 cycles

         Algorithm        mode Mbyte/s cycles/byte cycles/block
            memxor     aligned 1494.93        0.64         2.55
            memxor   unaligned  638.14        1.49         5.98
           memxor3     aligned 1273.34        0.75         3.00
           memxor3 unaligned01  717.56        1.33         5.32
           memxor3 unaligned11  695.91        1.37         5.48
           memxor3 unaligned12  478.54        1.99         7.97

               md2      update    2.43      392.50      6279.93
               md4      update  118.95        8.02       513.12
               md5      update   89.33       10.68       683.25
       openssl md5      update   93.94       10.15       649.75
              sha1      update   55.57       17.16      1098.34
      openssl sha1      update   55.19       17.28      1105.97
            sha224      update   24.60       38.76      2480.93
            sha256      update   24.62       38.73      2479.02
            sha384      update    7.77      122.67     15702.14
            sha512      update    7.77      122.67     15702.14
          sha3_224      update    5.79      164.69     23715.97
          sha3_256      update    5.47      174.27     23700.48
          sha3_384      update    4.19      227.80     23691.27
          sha3_512      update    2.90      328.64     23662.39
         ripemd160      update   46.25       20.62      1319.59
        gosthash94      update   10.05       94.90      3036.95

            aes128 ECB encrypt   17.33       55.03       880.47
            aes128 ECB decrypt   17.37       54.91       878.56
            aes128 CBC encrypt   15.17       62.85      1005.64
            aes128 CBC decrypt   17.03       56.00       895.96
            aes128         CTR   15.84       60.21       963.44

            aes192 ECB encrypt   14.43       66.10      1057.61
            aes192 ECB decrypt   14.45       66.01      1056.18
            aes192 CBC encrypt   12.96       73.58      1177.30
            aes192 CBC decrypt   14.23       67.02      1072.39
            aes192         CTR   13.44       70.94      1135.10

            aes256 ECB encrypt   12.49       76.35      1221.64
            aes256 ECB decrypt   12.50       76.29      1220.69
            aes256 CBC encrypt   11.32       84.22      1347.53
            aes256 CBC decrypt   12.31       77.49      1239.76
            aes256         CTR   11.72       81.37      1301.99

    openssl aes128 ECB encrypt   18.11       52.66       842.56
    openssl aes128 ECB decrypt   18.17       52.50       839.93

    openssl aes192 ECB encrypt   15.34       62.17       994.67
    openssl aes192 ECB decrypt   15.20       62.75      1003.97

    openssl aes256 ECB encrypt   13.28       71.82      1149.16
    openssl aes256 ECB decrypt   13.23       72.08      1153.22

        arcfour128 ECB encrypt   46.79       20.38
        arcfour128 ECB decrypt   46.78       20.39

openssl arcfour128 ECB encrypt   58.58       16.28
openssl arcfour128 ECB decrypt   58.46       16.31

       blowfish128 ECB encrypt   26.12       36.51       292.12
       blowfish128 ECB decrypt   28.94       32.95       263.62
       blowfish128 CBC encrypt   19.94       47.82       382.54
       blowfish128 CBC decrypt   28.02       34.04       272.33
       blowfish128         CTR   21.46       44.43       355.48

     openssl bf128 ECB encrypt   21.19       45.00       360.01
     openssl bf128 ECB decrypt   21.88       43.59       348.74

       camellia128 ECB encrypt    6.95      137.31      2197.01
       camellia128 ECB decrypt    6.95      137.31      2197.01
       camellia128 CBC encrypt    6.61      144.32      2309.07
       camellia128 CBC decrypt    6.88      138.58      2217.28
       camellia128         CTR    6.71      142.23      2275.69

       camellia192 ECB encrypt    5.21      182.99      2927.77
       camellia192 ECB decrypt    5.22      182.65      2922.40
       camellia192 CBC encrypt    5.03      189.73      3035.65
       camellia192 CBC decrypt    5.19      183.84      2941.48
       camellia192         CTR    5.09      187.42      2998.70

       camellia256 ECB encrypt    5.22      182.61      2921.81
       camellia256 ECB decrypt    5.22      182.69      2923.00
       camellia256 CBC encrypt    5.03      189.73      3035.65
       camellia256 CBC decrypt    5.18      183.95      2943.26
       camellia256         CTR    5.08      187.68      3002.87

           cast128 ECB encrypt   22.52       42.36       338.85
           cast128 ECB decrypt   23.31       40.92       327.34
           cast128 CBC encrypt   17.80       53.57       428.55
           cast128 CBC decrypt   22.67       42.07       336.52
           cast128         CTR   18.97       50.26       402.09

   openssl cast128 ECB encrypt   19.92       47.88       383.01
   openssl cast128 ECB decrypt   20.13       47.38       379.08

               des ECB encrypt   14.51       65.73       525.83
               des ECB decrypt   13.77       69.27       554.20
               des CBC encrypt   12.34       77.26       618.09
               des CBC decrypt   13.58       70.21       561.71
               des         CTR   12.73       74.94       599.50

       openssl des ECB encrypt   15.48       61.60       492.80
       openssl des ECB decrypt   15.16       62.90       503.18

              des3 ECB encrypt    4.66      204.82      1638.52
              des3 ECB decrypt    4.61      206.75      1654.02
              des3 CBC encrypt    4.28      222.96      1783.66
              des3 CBC decrypt    4.59      207.98      1663.86
              des3         CTR    4.44      214.69      1717.50

        serpent256 ECB encrypt   15.14       63.00      1008.02
        serpent256 ECB decrypt   16.49       57.85       925.53
        serpent256 CBC encrypt   13.81       69.05      1104.82
        serpent256 CBC decrypt   16.19       58.92       942.69
        serpent256         CTR   14.15       67.41      1078.59

        twofish128 ECB encrypt   21.78       43.79       700.70
        twofish128 ECB decrypt   22.52       42.34       677.45
        twofish128 CBC encrypt   19.17       49.75       796.07
        twofish128 CBC decrypt   21.94       43.46       695.33
        twofish128         CTR   19.68       48.46       775.32

        twofish192 ECB encrypt   21.81       43.72       699.51
        twofish192 ECB decrypt   22.52       42.34       677.45
        twofish192 CBC encrypt   19.16       49.77       796.30
        twofish192 CBC decrypt   21.94       43.46       695.33
        twofish192         CTR   19.66       48.52       776.28

        twofish256 ECB encrypt   21.78       43.79       700.70
        twofish256 ECB decrypt   22.53       42.33       677.21
        twofish256 CBC encrypt   19.17       49.75       796.07
        twofish256 CBC decrypt   21.94       43.47       695.45
        twofish256         CTR   19.65       48.54       776.63

           salsa20 ECB encrypt   39.69       24.03
           salsa20 ECB decrypt   39.70       24.02

           gcm-aes      update   43.64       21.86       349.69
           gcm-aes     encrypt   11.75       81.18      1298.89
           gcm-aes     decrypt   11.74       81.23      1299.61
