Implemented side-channel silent modp for most curves. Optimized modp
for the 521-bit curve:

On matrix (Intel i5, 3.4 GHz):

size       modp       modq    add_jja     dup_jj (us)
 192    0.06480    0.05748    1.11091    0.87571
 224    0.05133    0.09631    0.93989    0.78252
 256    0.07201    0.07392    1.18120    0.94241
 384    0.07825    0.10545    1.55906    1.25282
 521    0.03744    0.21319    1.51441    1.16038

 name size  sign / ms verify / ms
  rsa 1024     6.3203   103.4789
  rsa 2048     0.9567    29.5701
  dsa 1024    11.1913     5.7494
ecdsa  192     2.4718     1.8088
ecdsa  224     2.0912     1.7448
ecdsa  256     1.7018     1.2724
ecdsa  384     0.8548     0.6341
ecdsa  521     0.5741     0.4930

On pandix (ARM Cortex A9, 1 GHz):

size       modp       modq    add_jja     dup_jj (us)
 192    0.64133    0.77988   12.20131    9.31091
 224    0.68466    0.88974   13.91983   10.51140
 256    1.84937    1.01337   28.35083   21.08383
 384    1.01948    1.62201   25.16937   18.93616
 521    0.46883    3.24478   27.77863   19.98138

 name size  sign / ms verify / ms
  rsa 1024     0.2622     4.5472
  rsa 2048     0.0392     1.2488
  dsa 1024     0.4694     0.2377
ecdsa  192     0.2213     0.1690
ecdsa  224     0.1684     0.1274
ecdsa  256     0.0756     0.0562
ecdsa  384     0.0541     0.0416
ecdsa  521     0.0339     0.0282

---

After some optimization of modp for 384-bits:

On matrix (Intel i5, 3.4 GHz):

size       modp       modq    add_jja     dup_jj (us)
 192    0.07091    0.05668    1.09779    0.86758
 224    0.05318    0.09610    0.96975    0.75536
 256    0.07077    0.07371    1.14675    0.88859
 384    0.07690    0.10647    1.52311    1.17861
 521    0.03677    0.21338    1.48834    1.10846

 name size  sign / ms verify / ms
  rsa 1024     6.3189   103.0172
  rsa 2048     0.9561    29.4564
  dsa 1024    11.1924     5.7564
ecdsa  192     2.5343     1.8521
ecdsa  224     2.1165     1.7828
ecdsa  256     1.7860     1.3150
ecdsa  384     0.8958     0.6678
ecdsa  521     0.5864     0.5098

On pandix (ARM Cortex A9, 1 GHz):

size       modp       modq    add_jja     dup_jj (us)
 192    0.63873    0.76004   12.32910    9.40552
 224    0.68176    0.86761   14.06860   10.59914
 256    1.84631    0.99670   28.70941   21.23642
 384    0.76553    1.59912   21.93069   16.45279
 521    0.46928    3.22723   27.29797   19.56940

 name size  sign / ms verify / ms
  rsa 1024     0.2630     4.5487
  rsa 2048     0.0392     1.2488
  dsa 1024     0.4687     0.2375
ecdsa  192     0.2201     0.1685
ecdsa  224     0.1669     0.1266
ecdsa  256     0.0749     0.0557
ecdsa  384     0.0616     0.0481
ecdsa  521     0.0347     0.0288
