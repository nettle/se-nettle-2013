/* t-add.c */

#include "testutils.h"

int
main (int argc UNUSED, char **argv UNUSED)
{
  unsigned i;
  for (i = 0; curves[i]; i++)
    {
      const struct ecc_curve *ecc = curves[i];
      mp_limb_t *g1 = point_alloc (ecc);
      mp_limb_t *g2 = point_alloc (ecc);
      mp_limb_t *g2a = point_alloc (ecc);
      mp_limb_t *g3 = point_alloc (ecc);
      mp_limb_t *g4 = point_alloc (ecc);
      mp_limb_t *scratch = limbs_alloc (ecc_add_jjj_itch (ecc));
      const mp_limb_t *g;

      if (ecc->use_redc != 0)
	g = ecc->redc_g;
      else
	g = ecc->g;

      mpn_copyi (g1, g, 2*ecc->size);
      mpn_copyi (g1 + 2*ecc->size, ecc->unit, ecc->size);

      /* 2g, with ecc_dup_ja */
      ecc_dup_ja (ecc, g2a, g, scratch);
      check_mul_j (i, 2, g2a);
      ecc_j_to_a (ecc, 0, g2a, g2a, scratch);

      /* 2g, with ecc_dup_jj */
      ecc_dup_jj (ecc, g2, g1, scratch);
      check_mul_j (i, 2, g2);

      /* 3g = 2g+g, with ecc_add_jja */
      ecc_add_jja (ecc, g3, g2, g, scratch);
      check_mul_j (i, 3, g3);

      /* 3g = 2g+g, with ecc_add_jjj */
      ecc_add_jjj (ecc, g3, g2, g1, scratch);
      check_mul_j (i, 3, g3);
      
      /* 4g = 3g+g, with ecc_add_jja */
      ecc_add_jja (ecc, g4, g3, g, scratch);
      check_mul_j (i, 4, g4);

      /* 4g = 3g+g, with ecc_add_jjj */
      ecc_add_jjj (ecc, g4, g3, g1, scratch);
      check_mul_j (i, 4, g4);

      /* 4g = 2(2g), with ecc_dup_ja */
      ecc_dup_ja (ecc, g4, g2a, scratch);
      check_mul_j (i, 4, g4);
      
      /* 4g = 2(2g), with ecc_dup_jj */
      ecc_dup_jj (ecc, g4, g2, scratch);
      check_mul_j (i, 4, g4);

      free (g1);
      free (g2);
      free (g2a);
      free (g3);
      free (g4);
      free (scratch);
    }
  return EXIT_SUCCESS;
}
