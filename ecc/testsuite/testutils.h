/* testutils.h */

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "../ecdsa.h"
#include "../ecc-internal.h"
#include "../gmp-glue.h"

#if __GNUC__
# define NORETURN __attribute__ ((__noreturn__))
# define PRINTF_STYLE(f, a) __attribute__ ((__format__ (__printf__, f, a)))
# define UNUSED __attribute__ ((__unused__))
#else
# define NORETURN
# define PRINTF_STYLE(f, a)
# define UNUSED
#endif

extern const struct ecc_curve * const curves[];

void
die(const char *format, ...) PRINTF_STYLE (1, 2) NORETURN;

void *
xalloc (size_t size);

mp_limb_t *
limbs_alloc (size_t n);

mp_limb_t *
point_alloc (const struct ecc_curve *ecc);

void
check_mul_a (unsigned curve, unsigned n, const mp_limb_t *p);

void
check_mul_j (unsigned curve, unsigned n, const mp_limb_t *p);

size_t
hex2string (uint8_t **r, const char *hex);
