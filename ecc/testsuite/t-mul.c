/* t-add.c */

#include "testutils.h"

int
main (int argc UNUSED, char **argv UNUSED)
{
  gmp_randstate_t state;
  mpz_t r;
  unsigned i;

  gmp_randinit_default (state);
  mpz_init (r);
  
  for (i = 0; curves[i]; i++)
    {
      const struct ecc_curve *ecc = curves[i];
      mp_limb_t *p = point_alloc (ecc);
      mp_limb_t *q = point_alloc (ecc);
      mp_limb_t *n = limbs_alloc (ecc->size);
      mp_limb_t *scratch = limbs_alloc (ecc_mul_a_itch (ecc));
      unsigned j;
      
      mpn_zero (n, ecc->size);

      n[0] = 1;
      ecc_mul_a (ecc, 1, p, n, ecc->g, scratch);
      ecc_j_to_a (ecc, 1, p, p, scratch);

      if (mpn_cmp (p, ecc->g, 2*ecc->size != 0))
	{
	  die ("curve %d: ecc_mul_a with n = 1 failed.\n", ecc->bit_size);
	  abort ();
	}

      if (ecc->use_redc)
	{
	  ecc_mul_a (ecc, 0, p, n, ecc->redc_g, scratch);
	  ecc_j_to_a (ecc, 1, p, p, scratch);

	  if (mpn_cmp (p, ecc->g, 2*ecc->size != 0))
	    {
	      die ("curve %d: ecc_mul_a with n = 1 and redc failed.\n", ecc->bit_size);
	      abort ();
	    }
	}
      for (n[0] = 2; n[0] <= 4; n[0]++)
	{
	  ecc_mul_a (ecc, 1, p, n, ecc->g, scratch);
	  check_mul_j (i, n[0], p);
	  if (ecc->use_redc)
	    {
	      ecc_mul_a (ecc, 0, p, n, ecc->redc_g, scratch);
	      check_mul_j (i, n[0], p);
	    }
	}

      /* (order - 1) * g = - g */
      mpn_sub_1 (n, ecc->q, ecc->size, 1);
      ecc_mul_a (ecc, 1, p, n, ecc->g, scratch);
      ecc_j_to_a (ecc, 1, p, p, scratch);
      mpn_sub_n (p + ecc->size, ecc->p, p + ecc->size, ecc->size);
      if (mpn_cmp (p, ecc->g, 2*ecc->size) != 0)
	{
	  fprintf (stderr, "ecc_mul_a with n = order - 1 failed.\n");
	  abort ();
	}

      mpn_zero (n, ecc->size);

      n[0] = 1;
      ecc_mul_g (ecc, p, n, scratch);
      ecc_j_to_a (ecc, 1, p, p, scratch);

      if (mpn_cmp (p, ecc->g, 2*ecc->size != 0))
	{
	  fprintf (stderr, "ecc_mul_g with n = 1 failed.\n");
	  abort ();
	}

      for (n[0] = 2; n[0] <= 4; n[0]++)
	{
	  ecc_mul_g (ecc, p, n, scratch);
	  check_mul_j (i, n[0], p);
	}

      /* (order - 1) * g = - g */
      mpn_sub_1 (n, ecc->q, ecc->size, 1);
      ecc_mul_g (ecc, p, n, scratch);
      ecc_j_to_a (ecc, 1, p, p, scratch);
      mpn_sub_n (p + ecc->size, ecc->p, p + ecc->size, ecc->size);
      if (mpn_cmp (p, ecc->g, 2*ecc->size) != 0)
	{
	  fprintf (stderr, "ecc_mul_g with n = order - 1 failed.\n");
	  abort ();
	}

      for (j = 0; j < 100; j++)
	{
	  if (j & 1)
	    mpz_rrandomb (r, state, ecc->size * GMP_NUMB_BITS);
	  else
	    mpz_urandomb (r, state, ecc->size * GMP_NUMB_BITS);

	  /* Reduce so that (almost surely) n < q */
	  _mpz_copy_limbs (n, r, ecc->size);
	  n[ecc->size - 1] %= ecc->q[ecc->size - 1];

	  ecc_mul_a (ecc, 1, p, n, ecc->g, scratch);
	  ecc_j_to_a (ecc, 1, p, p, scratch);

	  ecc_mul_g (ecc, q, n, scratch);
	  ecc_j_to_a (ecc, 1, q, q, scratch);

	  if (mpn_cmp (p, q, 2*ecc->size))
	    {
	      gmp_fprintf (stderr,
			   "Different results from ecc_mul_a and ecc_mul_g.\n"
			   " bits = %u\n"
			   " n = %Nx\n",
			   ecc->bit_size, n, ecc->size);
	      gmp_fprintf (stderr, "p = %Nx,\n    %Nx\n",
			   p, ecc->size, p + ecc->size, ecc->size);
	      gmp_fprintf (stderr, "q = %Nx,\n    %Nx\n",
			   q, ecc->size, q + ecc->size, ecc->size);
	      abort ();
	    }
	}
      free (n);
      free (p);
      free (q);
      free (scratch);
    }
  mpz_clear (r); 
  gmp_randclear (state);

  return 0;
}
