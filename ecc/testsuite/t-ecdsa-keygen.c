/* t-ecdsa-keygen.c */

#include <string.h>

#include <nettle/knuth-lfib.h>

#include "testutils.h"

/* Check if y^2 = x^3 - 3x + b */
static int
ecc_valid_p (struct ecdsa_public_key *pub)
{
  mpz_t t, x, y;
  mpz_t lhs, rhs;
  int res;
  mp_size_t size;

  size = pub->ecc->size;

  /* First check range */
  if (mpn_cmp (pub->Y, pub->ecc->p, size) >= 0
      || mpn_cmp (pub->Y + size, pub->ecc->p, size) >= 0)
    return 0;

  mpz_init (lhs);
  mpz_init (rhs);

  _mpz_init_mpn (x, pub->Y, size);
  _mpz_init_mpn (y, pub->Y + size, size);

  mpz_mul (lhs, y, y);
  mpz_mul (rhs, x, x);
  mpz_sub_ui (rhs, rhs, 3);
  mpz_mul (rhs, rhs, x);
  mpz_add (rhs, rhs, _mpz_init_mpn (t, pub->ecc->b, size));

  res = mpz_congruent_p (lhs, rhs, _mpz_init_mpn (t, pub->ecc->p, size));
  
  mpz_clear (lhs);
  mpz_clear (rhs);

  return res;
}

int
main (int argc UNUSED, char **argv UNUSED)
{
  unsigned i;
  struct knuth_lfib_ctx rctx;
  struct dsa_signature signature;

  uint8_t *digest;
  size_t digest_length;
  int verbose = 0;

  knuth_lfib_init (&rctx, 4711);
  dsa_signature_init (&signature);

  digest_length = hex2string (&digest, /* sha256("abc") */
			      "BA7816BF 8F01CFEA 414140DE 5DAE2223"
			      "B00361A3 96177A9C B410FF61 F20015AD");

  for (i = 0; curves[i]; i++)
    {
      const struct ecc_curve *ecc = curves[i];
      struct ecdsa_public_key pub;
      struct ecdsa_private_key key;

      if (verbose)
	fprintf (stderr, "Curve %d\n", ecc->bit_size);

      ecdsa_public_key_init (&pub, ecc);
      ecdsa_private_key_init (&key, ecc);

      ecdsa_generate_keypair (&pub, &key,
			      &rctx,
			      (nettle_random_func *) knuth_lfib_random);

      if (verbose)
	{
	  gmp_fprintf (stderr,
		       "Public key:\nx = %Nx\ny = %Nx\n",
		       pub.Y, ecc->size, pub.Y + ecc->size, ecc->size);
	  gmp_fprintf (stderr,
		       "Private key: %Nx\n", key.z, ecc->size);
	}
      if (!ecc_valid_p (&pub))
	die ("ecdsa_generate_keypair produced an invalid point.\n");

      ecdsa_sign (&key,
		  &rctx, (nettle_random_func *) knuth_lfib_random,
		  digest_length, digest,
		  &signature);

      if (!ecdsa_verify (&pub, digest_length, digest,
			  &signature))
	die ("ecdsa_verify failed.\n");

      digest[3] ^= 17;
      if (ecdsa_verify (&pub, digest_length, digest,
			 &signature))
	die ("ecdsa_verify  returned success with invalid digest.\n");
      digest[3] ^= 17;

      mpz_combit (signature.r, 117);
      if (ecdsa_verify (&pub, digest_length, digest,
			 &signature))
	die ("ecdsa_verify  returned success with invalid signature.r.\n");

      mpz_combit (signature.r, 117);
      mpz_combit (signature.s, 93);
      if (ecdsa_verify (&pub, digest_length, digest,
			 &signature))
	die ("ecdsa_verify  returned success with invalid signature.s.\n");

      ecdsa_public_key_clear (&pub);
      ecdsa_private_key_clear (&key);
    }
  free (digest);
  dsa_signature_clear (&signature);

  return 0;
}
