/* ecc-modp.c */

/* nettle, low-level cryptographics library
 *
 * Copyright (C) 2013 Niels Möller
 *  
 * The nettle library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 * 
 * The nettle library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the nettle library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02111-1301, USA.
 */

/* Development of Nettle's ECC support was funded by Internetfonden. */

#include <assert.h>

#include "ecc-internal.h"

/* Routines for modp arithmetic. Outputs are ecc->size limbs, but not
   necessarily < p. */

mp_limb_t
add_1_sec (mp_limb_t *rp, mp_limb_t *ap, mp_size_t n, mp_limb_t b)
{
  mp_size_t i;
  for (i = 0; i < n; i++)
    {
      mp_limb_t r = ap[i] + b;
      b = (r < b);
      rp[i] = r;
    }
  return b;
}

mp_limb_t
sub_1_sec (mp_limb_t *rp, mp_limb_t *ap, mp_size_t n, mp_limb_t b)
{
  mp_size_t i;
  for (i = 0; i < n; i++)
    {
      mp_limb_t a;
      a = ap[i];
      rp[i] = a - b;
      b = a < b;
    }
  return b;
}


/* Computes r mod m, where m is of size mn. bp holds B^mn mod m, as mn
   limbs, but the upper mn - bn libms are zero. */
static void
ecc_mod (mp_limb_t *rp, mp_size_t rn, mp_size_t mn,
	 const mp_limb_t *bp, mp_size_t bn,
	 const mp_limb_t *b_shifted, unsigned shift)
{
  mp_limb_t hi;
  mp_size_t sn = mn - bn;
  mp_size_t i;

  assert (sn > 0);

  /* FIXME: Could use mpn_addmul_2. */
  /* Eliminate sn = mn - bn limbs at a time */
  if (bp[bn-1] < ((mp_limb_t) 1 << (GMP_NUMB_BITS - 1)))
    {
      /* Multiply sn + 1 limbs at a time, so we get a mn+1 limb
	 product. Then we can absorb the carry in the high limb */
      while (rn > 2 * mn - bn)
	{
	  rn -= sn;

	  for (i = 0; i <= sn; i++)
	    rp[rn+i-1] = mpn_addmul_1 (rp + rn - mn - 1 + i, bp, bn, rp[rn+i-1]);
	  rp[rn-1] = rp[rn+sn-1]
	    + mpn_add_n (rp + rn - sn - 1, rp + rn - sn - 1, rp + rn - 1, sn);
	}
      goto final_limbs;
    }
  else
    {
      while (rn >= 2 * mn - bn)
	{
	  rn -= sn;

	  for (i = 0; i < sn; i++)
	    rp[rn+i] = mpn_addmul_1 (rp + rn - mn + i, bp, bn, rp[rn+i]);
				     
	  hi = mpn_add_n (rp + rn - sn, rp + rn - sn, rp + rn, sn);
	  hi = cnd_add_n (hi, rp + rn - mn, bp, mn);
	  assert (hi == 0);
	}
    }

  if (rn > mn)
    {
    final_limbs:
      sn = rn - mn;
      
      for (i = 0; i < sn; i++)
	rp[mn+i] = mpn_addmul_1 (rp + i, bp, bn, rp[mn+i]);

      hi = mpn_add_n (rp + bn, rp + bn, rp + mn, sn);
      hi = add_1_sec (rp + bn + sn, rp + bn + sn, mn - bn - sn, hi);
    }

  if (shift > 0)
    {
      /* Combine hi with top bits, add in */
      hi = (hi << shift) | (rp[mn-1] >> (GMP_NUMB_BITS - shift));
      rp[mn-1] = (rp[mn-1] & (((mp_limb_t) 1 << (GMP_NUMB_BITS - shift)) - 1))
	+ mpn_addmul_1 (rp, b_shifted, mn-1, hi);
    }
  else
    {
      hi = cnd_add_n (hi, rp, bp, mn);
      assert (hi == 0);
    }
}

void
ecc_generic_modp (const struct ecc_curve *ecc, mp_limb_t *rp)
{
  assert (ecc->Bmodp_size < ecc->size);
  
  ecc_mod (rp, 2*ecc->size, ecc->size, ecc->Bmodp, ecc->Bmodp_size,
	   ecc->Bmodp_shifted,
	   ecc->size * GMP_NUMB_BITS - ecc->bit_size);
}

void
ecc_generic_redc (const struct ecc_curve *ecc, mp_limb_t *rp)
{
  unsigned i;
  mp_limb_t hi, cy;
  unsigned shift = ecc->size * GMP_NUMB_BITS - ecc->bit_size;
  mp_size_t k = ecc->redc_size;
  
  assert (k != 0);
  if (k > 0)    
    {
      /* Use that 1 = p + 1, and that at least one low limb of p + 1 is zero. */
      for (i = 0; i < ecc->size; i++)
	rp[i] = mpn_addmul_1 (rp + i + k,
			      ecc->redc_ppm1, ecc->size - k, rp[i]);
      hi = mpn_add_n (rp, rp, rp + ecc->size, ecc->size);
      if (shift > 0)
	{
	  hi = (hi << shift) | (rp[ecc->size - 1] >> (GMP_NUMB_BITS - shift));
	  rp[ecc->size - 1] = (rp[ecc->size - 1]
			       & (((mp_limb_t) 1 << (GMP_NUMB_BITS - shift)) - 1))
	    + mpn_addmul_1 (rp, ecc->Bmodp_shifted, ecc->size-1, hi);
	  
	}
      else
	{
	  cy = cnd_sub_n (hi, rp, ecc->p, ecc->size);
	  assert (cy == hi);      
	}
    }
  else
    {
      /* Use that 1 = - (p - 1), and that at least one low limb of p -
	 1 is zero. */
      k = -k;
      for (i = 0; i < ecc->size; i++)
	rp[i] = mpn_submul_1 (rp + i + k,
			      ecc->redc_ppm1, ecc->size - k, rp[i]);
      hi = mpn_sub_n (rp, rp + ecc->size, rp, ecc->size);
      cy = cnd_add_n (hi, rp, ecc->p, ecc->size);
      assert (cy == hi);

      if (shift > 0)
	{
	  /* Result is always < 2p, provided that
	     2^shift * Bmodp_shifted <= p */
	  hi = (rp[ecc->size - 1] >> (GMP_NUMB_BITS - shift));
	  rp[ecc->size - 1] = (rp[ecc->size - 1]
			       & (((mp_limb_t) 1 << (GMP_NUMB_BITS - shift)) - 1))
	    + mpn_addmul_1 (rp, ecc->Bmodp_shifted, ecc->size-1, hi);
	}
    }
}

void
ecc_modp_add (const struct ecc_curve *ecc, mp_limb_t *rp,
	      const mp_limb_t *ap, const mp_limb_t *bp)
{
  mp_limb_t cy;
  cy = mpn_add_n (rp, ap, bp, ecc->size);
  cy = cnd_add_n (cy, rp, ecc->Bmodp, ecc->size);
  cy = cnd_add_n (cy, rp, ecc->Bmodp, ecc->size);
  assert (cy == 0);  
}

void
ecc_modp_sub (const struct ecc_curve *ecc, mp_limb_t *rp,
	      const mp_limb_t *ap, const mp_limb_t *bp)
{
  mp_limb_t cy;
  cy = mpn_sub_n (rp, ap, bp, ecc->size);
  cy = cnd_sub_n (cy, rp, ecc->Bmodp, ecc->size);
  cy = cnd_sub_n (cy, rp, ecc->Bmodp, ecc->size);
  assert (cy == 0);  
}

void
ecc_modp_sub_1 (const struct ecc_curve *ecc, mp_limb_t *rp,
		const mp_limb_t *ap, mp_limb_t b)
{
  mp_size_t i;

  for (i = 0; i < ecc->size; i++)
    {
      mp_limb_t cy = ap[i] < b;
      rp[i] = ap[i] - b;
      b = cy;
    }
  b = cnd_sub_n (b, rp, ecc->Bmodp, ecc->size);
  assert (b == 0);    
}

void
ecc_modp_mul_1 (const struct ecc_curve *ecc, mp_limb_t *rp,
		const mp_limb_t *ap, mp_limb_t b)
{
  mp_limb_t hi;

  assert (b <= 0xffffffff);
  hi = mpn_mul_1 (rp, ap, ecc->size, b);
  hi = mpn_addmul_1 (rp, ecc->Bmodp, ecc->size, hi);
  assert (hi <= 1);
  hi = cnd_add_n (hi, rp, ecc->Bmodp, ecc->size);
  /* Sufficient if b < B^size / p */
  assert (hi == 0);
}

void
ecc_modp_addmul_1 (const struct ecc_curve *ecc, mp_limb_t *rp,
		   const mp_limb_t *ap, mp_limb_t b)
{
  mp_limb_t hi;

  assert (b <= 0xffffffff);
  hi = mpn_addmul_1 (rp, ap, ecc->size, b);
  hi = mpn_addmul_1 (rp, ecc->Bmodp, ecc->size, hi);
  assert (hi <= 1);
  hi = cnd_add_n (hi, rp, ecc->Bmodp, ecc->size);
  /* Sufficient roughly if b < B^size / p */
  assert (hi == 0);
}
  
void
ecc_modp_submul_1 (const struct ecc_curve *ecc, mp_limb_t *rp,
		   const mp_limb_t *ap, mp_limb_t b)
{
  mp_limb_t hi;

  assert (b <= 0xffffffff);
  hi = mpn_submul_1 (rp, ap, ecc->size, b);
  hi = mpn_submul_1 (rp, ecc->Bmodp, ecc->size, hi);
  assert (hi <= 1);
  hi = cnd_sub_n (hi, rp, ecc->Bmodp, ecc->size);
  /* Sufficient roughly if b < B^size / p */
  assert (hi == 0);
}

/* NOTE: mul and sqr needs 2*ecc->size limbs at rp */
void
ecc_modp_mul (const struct ecc_curve *ecc, mp_limb_t *rp,
	      const mp_limb_t *ap, const mp_limb_t *bp)
{
  mpn_mul_n (rp, ap, bp, ecc->size);
  ecc->reduce (ecc, rp);
}

void
ecc_modp_sqr (const struct ecc_curve *ecc, mp_limb_t *rp,
	      const mp_limb_t *ap)
{
  mpn_sqr (rp, ap, ecc->size);
  ecc->reduce (ecc, rp);
}


/* Arithmetic mod q (the group order). FIXME: Reorganize, to reduce
   code duplication? */

void
ecc_generic_modq (const struct ecc_curve *ecc, mp_limb_t *rp)
{
  assert (ecc->Bmodq_size < ecc->size);

  ecc_mod (rp, 2*ecc->size, ecc->size, ecc->Bmodq, ecc->Bmodq_size,
	   ecc->Bmodq_shifted,
	   ecc->size * GMP_NUMB_BITS - ecc->bit_size);
}

void
ecc_modq_add (const struct ecc_curve *ecc, mp_limb_t *rp,
	      const mp_limb_t *ap, const mp_limb_t *bp)
{
  mp_limb_t cy;
  cy = mpn_add_n (rp, ap, bp, ecc->size);
  cy = cnd_add_n (cy, rp, ecc->Bmodq, ecc->size);
  cy = cnd_add_n (cy, rp, ecc->Bmodq, ecc->size);
  assert (cy == 0);  
}

void
ecc_modq_mul (const struct ecc_curve *ecc, mp_limb_t *rp,
	      const mp_limb_t *ap, const mp_limb_t *bp)
{
  mpn_mul_n (rp, ap, bp, ecc->size);
  ecc->modq (ecc, rp);
}


/* Modular inversion */

static void
cnd_neg (int cnd, mp_limb_t *rp, const mp_limb_t *ap, mp_size_t n)
{
  mp_limb_t cy = (cnd != 0);
  mp_limb_t mask = -cy;
  mp_size_t i;

  for (i = 0; i < n; i++)
    {
      mp_limb_t r = (ap[i] ^ mask) + cy;
      cy = r < cy;
      rp[i] = r;
    }
}

static void
cnd_swap (int cnd, mp_limb_t *ap, mp_limb_t *bp, mp_size_t n)
{
  mp_limb_t mask = - (mp_limb_t) (cnd != 0);
  mp_size_t i;
  for (i = 0; i < n; i++)
    {
      mp_limb_t a, b, t;
      a = ap[i];
      b = bp[i];
      t = (a ^ b) & mask;
      ap[i] = a ^ t;
      bp[i] = b ^ t;
    }
}

/* Compute a^{-1} mod m, with running time depending only on the size.
   Also needs (m+1)/2, and m must be odd. */
static void
modinv_sec (mp_limb_t *vp, mp_limb_t *ap, mp_size_t n,
	    const mp_limb_t *mp, const mp_limb_t *mp1h, mp_size_t bit_size,
	    mp_limb_t *scratch)
{
#define bp scratch
#define dp (scratch + n)
#define up (scratch + 2*n)

  mp_bitcnt_t i;

  /* Maintain

       a = u * orig_a (mod m)
       b = v * orig_a (mod m)

     and b odd at all times. Initially,

       a = a_orig, u = 1
       b = m,      v = 0
     */

  assert (ap != vp);

  up[0] = 1;
  mpn_zero (up+1, n - 1);
  mpn_copyi (bp, mp, n);
  mpn_zero (vp, n);

  for (i = bit_size + GMP_NUMB_BITS * n; i-- > 0; )
    {
      mp_limb_t odd, swap, cy;
      
      /* Always maintain b odd. The logic of the iteration is as
	 follows. For a, b:

	   odd = a & 1
	   a -= odd * b
	   if (underflow from a-b)
	     {
	       b += a, assigns old a
	       a = B^n-a
	     }
	   
	   a /= 2

	 For u, v:

	   if (underflow from a - b)
	     swap u, v
	   u -= odd * v
	   if (underflow from u - v)
	     u += m

	   u /= 2
	   if (a one bit was shifted out)
	     u += (m+1)/2

	 As long as a > 0, the quantity

	   (bitsize of a) + (bitsize of b)

	 is reduced by at least one bit per iteration, hence after
         (bit_size of orig_a) + (bit_size of m) - 1 iterations we
         surely have a = 0. Then b = gcd(orig_a, m) and if b = 1 then
         also v = orig_a^{-1} (mod m)
      */

      assert (bp[0] & 1);
      odd = ap[0] & 1;

      /* Which variant is fastest depends on the speed of the various
	 cnd_* functions. Assembly implementation would help. */
#if 1
      swap = cnd_sub_n (odd, ap, bp, n);
      cnd_add_n (swap, bp, ap, n);
      cnd_neg (swap, ap, ap, n);
#else
      swap = odd & mpn_sub_n (dp, ap, bp, n);
      cnd_copy (swap, bp, ap, n);
      cnd_neg (swap, dp, dp, n);
      cnd_copy (odd, ap, dp, n);
#endif

#if 1
      cnd_swap (swap, up, vp, n);
      cy = cnd_sub_n (odd, up, vp, n);
      cy -= cnd_add_n (cy, up, mp, n);
#else
      cy = cnd_sub_n (odd, up, vp, n);
      cnd_add_n (swap, vp, up, n);
      cnd_neg (swap, up, up, n);
      cnd_add_n (cy ^ swap, up, mp, n);
#endif
      cy = mpn_rshift (ap, ap, n, 1);
      assert (cy == 0);
      cy = mpn_rshift (up, up, n, 1);
      cy = cnd_add_n (cy, up, mp1h, n);
      assert (cy == 0);
    }
  assert ( (ap[0] | ap[n-1]) == 0);
#undef bp
#undef dp
#undef up
}

void
ecc_modp_inv (const struct ecc_curve *ecc, mp_limb_t *rp, mp_limb_t *ap,
	      mp_limb_t *scratch)
{
  modinv_sec (rp, ap, ecc->size, ecc->p, ecc->pp1h, ecc->bit_size, scratch);
}

void
ecc_modq_inv (const struct ecc_curve *ecc, mp_limb_t *rp, mp_limb_t *ap,
	      mp_limb_t *scratch)
{
  modinv_sec (rp, ap, ecc->size, ecc->q, ecc->qp1h, ecc->bit_size, scratch);
}
