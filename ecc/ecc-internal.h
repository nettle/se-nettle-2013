/* ecc-internal.h */

/* nettle, low-level cryptographics library
 *
 * Copyright (C) 2013 Niels Möller
 *  
 * The nettle library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 * 
 * The nettle library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the nettle library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02111-1301, USA.
 */

/* Development of Nettle's ECC support was funded by Internetfonden. */

#ifndef NETTLE_ECC_INTERNAL_H_INCLUDED
#define NETTLE_ECC_INTERNAL_H_INCLUDED

#if __GNUC__
# define NORETURN __attribute__ ((__noreturn__))
# define PRINTF_STYLE(f, a) __attribute__ ((__format__ (__printf__, f, a)))
# define UNUSED __attribute__ ((__unused__))
#else
# define NORETURN
# define PRINTF_STYLE(f, a)
# define UNUSED
#endif

#include "ecc.h"

/* Window size for ecc_mul_a. Using 4 bits seems like a good choice,
   for both Intel x86_64 and ARM Cortex A9. For the larger curves, of
   384 and 521 bits, we could improve seepd by a few percent if we go
   up to 5 bits, but I don't think that's worth doubling the
   storage. */
#define ECC_MUL_A_WBITS 4

/* Reduces from 2*ecc->size to ecc->size. */
/* Required to return a result < 2q. This property is inherited by
   modp_mul and modp_add. */
typedef void ecc_mod_func (const struct ecc_curve *ecc, mp_limb_t *rp);

/* Represents an elliptic curve of the form

     y^2 = x^3 - 3x + b (mod p)
*/
struct ecc_curve
{
  unsigned short bit_size;
  /* Limb size of elements in the base field, size of a point is
     2*size in affine coordinates and 3*size in jacobian
     coordinates. */
  unsigned short size;
  unsigned short Bmodp_size;
  unsigned short Bmodq_size;
  unsigned short use_redc;
  /* +k if p+1 has k low zero limbs, -k if p-1 has k low zero
     limbs. */
  short redc_size;
  unsigned short pippenger_k;
  unsigned short pippenger_c;

  /* The prime p. */
  const mp_limb_t *p;
  const mp_limb_t *b;
  /* Group order. */
  const mp_limb_t *q;
  /* Generator, x coordinate followed by y (affine coordinates). */
  const mp_limb_t *g;
  /* Generator with coordinates in Montgomery form. */
  const mp_limb_t *redc_g;

  ecc_mod_func *modp;
  ecc_mod_func *redc;
  ecc_mod_func *reduce;
  ecc_mod_func *modq;

  /* B^size mod p. Expected to have at least 32 leading zeros
     (equality for secp_256r1). */
  const mp_limb_t *Bmodp;
  /* 2^{bit_size} - p, same value as above, but shifted. */
  const mp_limb_t *Bmodp_shifted;
  /* (p+1)/2 */
  const mp_limb_t *pp1h;
  /* p +/- 1, for redc, excluding |redc_size| low limbs. */
  const mp_limb_t *redc_ppm1;
  /* For redc, same as Bmodp, otherwise 1. */
  const mp_limb_t *unit;

  /* Similarly, B^size mod q */
  const mp_limb_t *Bmodq;
  /* 2^{bit_size} - q, same value as above, but shifted. */
  const mp_limb_t *Bmodq_shifted;
  /* (q+1)/2 */
  const mp_limb_t *qp1h;
  
  /* Tables for multiplying by the generator, size determined by k and
     c. The first 2^c entries are defined by

       T[  j_0 +   j_1 2 +     ... + j_{c-1} 2^{c-1} ]
         = j_0 g + j_1 2^k g + ... + j_{c-1} 2^{k(c-1)} g

     The following entries differ by powers of 2^{kc},

       T[i] = 2^{kc} T[i-2^c]
  */  
  const mp_limb_t *pippenger_table;
};

/* In-place reduction. */
ecc_mod_func ecc_generic_modp;
ecc_mod_func ecc_generic_redc;
ecc_mod_func ecc_generic_modq;


void
ecc_modp_add (const struct ecc_curve *ecc, mp_limb_t *rp,
	      const mp_limb_t *ap, const mp_limb_t *bp);
void
ecc_modp_sub (const struct ecc_curve *ecc, mp_limb_t *rp,
	      const mp_limb_t *ap, const mp_limb_t *bp);

void
ecc_modp_sub_1 (const struct ecc_curve *ecc, mp_limb_t *rp,
		const mp_limb_t *ap, mp_limb_t b);

void
ecc_modp_mul_1 (const struct ecc_curve *ecc, mp_limb_t *rp,
		const mp_limb_t *ap, const mp_limb_t b);

void
ecc_modp_addmul_1 (const struct ecc_curve *ecc, mp_limb_t *rp,
		   const mp_limb_t *ap, mp_limb_t b);
void
ecc_modp_submul_1 (const struct ecc_curve *ecc, mp_limb_t *rp,
		   const mp_limb_t *ap, mp_limb_t b);

/* NOTE: mul and sqr needs 2*ecc->size limbs at rp */
void
ecc_modp_mul (const struct ecc_curve *ecc, mp_limb_t *rp,
	      const mp_limb_t *ap, const mp_limb_t *bp);

void
ecc_modp_sqr (const struct ecc_curve *ecc, mp_limb_t *rp,
	      const mp_limb_t *ap);

void
ecc_modp_inv (const struct ecc_curve *ecc, mp_limb_t *rp, mp_limb_t *ap,
	      mp_limb_t *scratch);

/* mod q operations. */
void
ecc_modq_mul (const struct ecc_curve *ecc, mp_limb_t *rp,
	      const mp_limb_t *ap, const mp_limb_t *bp);
void
ecc_modq_add (const struct ecc_curve *ecc, mp_limb_t *rp,
	      const mp_limb_t *ap, const mp_limb_t *bp);

void
ecc_modq_inv (const struct ecc_curve *ecc, mp_limb_t *rp, mp_limb_t *ap,
	      mp_limb_t *scratch);

#define cnd_add_n(cnd, rp, ap, n)		\
  mpn_addmul_1 ((rp), (ap), (n), (cnd) != 0)

#define cnd_sub_n(cnd, rp, ap, n)		\
  mpn_submul_1 ((rp), (ap), (n), (cnd) != 0)

/* FIXME: Defined in ecc-mul-binary.c. Move elsewhere? */
void
cnd_copy (int cnd, mp_limb_t *rp, const mp_limb_t *ap, mp_size_t n);

/* Defined in ecc-modp.c */
mp_limb_t
add_1_sec (mp_limb_t *rp, mp_limb_t *ap, mp_size_t n, mp_limb_t b);

mp_limb_t
sub_1_sec (mp_limb_t *rp, mp_limb_t *ap, mp_size_t n, mp_limb_t b);

/* Defined in ec-mul-g.c */
void
table_copy (mp_limb_t *rp, mp_size_t rn,
	    const mp_limb_t *table, unsigned tn,
	    unsigned k);

#define TMP_ALLOC_LIMBS(size) \
  ((mp_limb_t *) alloca ((size) * sizeof (mp_limb_t)))

/* Current scratch needs: */
#define ECC_MODINV_ITCH(size) (3*(size))
#define ECC_J_TO_A_ITCH(size) (5*(size))
#define ECC_DUP_JA_ITCH(size) (5*(size))
#define ECC_DUP_JJ_ITCH(size) (5*(size))
#define ECC_ADD_JJA_ITCH(size) (6*(size))
#define ECC_ADD_JJJ_ITCH(size) (8*(size))
#define ECC_MUL_G_ITCH(size) (9*(size))
#if ECC_MUL_A_WBITS == 0
#define ECC_MUL_A_ITCH(size) (12*(size))
#else
#define ECC_MUL_A_ITCH(size) \
  (((3 << ECC_MUL_A_WBITS) + 11) * (size))
#endif
#define _ECDSA_SIGN_ITCH(size) (12*(size))
#define _ECDSA_VERIFY_ITCH(size) \
  (6*(size) + ECC_MUL_A_ITCH ((size)))

#endif /* NETTLE_ECC_INTERNAL_H_INCLUDED */
