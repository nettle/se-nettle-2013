private int output_bits = 8;

void
set_output_bit_size(int bits)
{
  output_bits = bits;
}

/* Little endian */
void output_limbs(int x, int size)
{
  int mask = (1 << output_bits) - 1;
  for (int i; i < size > 0; i++)
    {
      if (i && ! (i%8))
	write("\n  ");
      write ("0x%02x,", x & mask);
      x >>= output_bits;
    }
  if (x)
    error("size too small!");
}

void output_bignum(int x, int size)
{
  write("{");
  output_limbs(x, size);
  write("}");
};

void output_mod_pre(int x, int size)
{
  write("{");
  output_limbs(x, size);
  write("\n");
  
  for (int i = 7; i > 0; i--)
    {
      write("\n  ");
      output_limbs(x << i, size+1);
    }
  write("}");
}

class point
{
  int x;
  int y;

  void create (int nx, int ny)
  {
    x = nx; y = ny;
  }
      
  int zero_p ()
  {
    return x == 0 && y == 0;
  }
  void output (int size)
  {
    write("{\n  ");
    output_bignum(x, size);
    write(",\n  "); 
    output_bignum(y, size);
    write("\n}");
  }
  pointh to_homogenous()
  {
    if (zero_p())
      return pointh(0,1,0);
    else
      return pointh(x, y, 1);
  }
};

class pointh
{
  inherit point;
  int z;

  void create (int nx, int ny, int nz)
  {
    x = nx; y = ny; z = nz;
  }
      
  int zero_p ()
  {
    return z == 0;
  }
  void output (int size)
  {
    write("{\n  ");
    output_bignum(x, size);
    write(",\n  "); 
    output_bignum(y, size);
    write(",\n  "); 
    output_bignum(z, size);
    write("\n}");
  }
};

class curve
{
  int m;
  int a;
  int b;
  point g;
  int n;

  point dup(point p)
  {
    if (!p->y)
      return point(0,0);

    int t = ((3*p->x*p->x + a) * (2*p->y)->invert(m)) % m;
    int x = (t*t - 2 * p->x) % m;
    int y = ((p->x - x) * t - p->y) % m;

    return point(x, y);
  }

  point add(point p, point q)
  {
    if (p->zero_p())
      return q;
    else if (q->zero_p())
      return p;
    else if (p->x == q->x)
      {
	if (p->y == q->y)
	  return dup(p);
	else
	  return point(0,0);
      }
    else
      {
	int t = (q->y - p->y) * (q->x - p->x)->invert(m) % m;
	int x = (t*t - p->x - q->x) % m;
	int y = ((p->x - x) * t - p->y) % m;

	return point(x, y);
      }
  }

  pointh duph(pointh p, int|void debug)
  {
    int t1 = 2*p->y * p->z % m;		/* 2 z y */
    /* If a == -3, use w = 3 (x-z)(x+z). */
    int w = (3*p->x*p->x + a * p->z*p->z) % m;
    int t2 = t1 * p->y % m;		/* 2 z y^2 */
    int t3 = t2 * p->x % m;		/* 2 z y^2 x */
    int t4 = w * w % m;			/* (3 x^2 + a z^2)^2 */

    int x = t1 * (t4 - 4 * t3) % m;	/* 2zy (w^2 - 8 z y^2 x) */
    int t6 = w * (6*t3 - t4) % m;	/* w (12 z y^2 x - w^2) */
    /* NOTE: Equation 4.4ii in Chudnovsky seems to be missing a factor
       of two in the first term for y! */
    int y = (- 2*t2*t2 + t6) % m;	/* - 2 (z y^2)^2
					   + w (12 z y^2 x - w^2) */
    int t5 = t1 * t1 % m;		/* (2 z y)^2 */
    int z = t1 * t5 % m;		/* (2 z y)^3 */
    if (debug)
      {
	werror("duph: in: (%x, %x, %x)\n"
	       "out: (%x, %x, %x)\n",
	       p->x, p->y, p->z, x, y, z);
	werror("w = %x\n"
	       "t1 = %x\n"
	       "t2 = %x\n"
	       "t3 = %x\n"
	       "t4 = %x\n"
	       "t5 = %x\n"
	       "t6 = %x\n",
	       w, t1, t2, t3, t4, t5, t6);
      }
    return pointh(x, y, z);
  }

  pointh addh(pointh p, pointh q, int|void debug)
  {
    if (p->zero_p())
      return q;
    else if (q->zero_p())
      return p;
    else
      {
	int r1 = p->x * q->z % m;
	int r2 = q->x * p->z % m;
	int v = (r2 - r1) % m;
	int s1 = p->y * q->z % m;
	int s2 = q->y * p->z % m;	
	int u = (s2 - s1) % m;

	if (u == 0 && v == 0)
	  return duph(p);

	int w = p->z * q->z % m;
	int t1 = v * v % m;
	int t2 = (r1 + r2) * t1 % m; /* (r1 + r2) v^2 */
	int t3 = u * u % m;
	int t4 = w * t3 % m; /* w u^2 */
	int x = v * (- t2 + t4) % m; /* v (-(r1 + r2) v^2 + w u^2) */
	int t5 = u * (3*t2-2*t4) % m; /* u (+ 3 (r1 + r2) v^2
					      -2 w u^2) */
	int t6 = t1 * v % m; /* v^3 */
	int y = t5 - t6*(s1+s2) % m;	/* u (-2wu^2 + 3(r1+r2)v^2)
					   -v^3(s1+s2) */
	/* Divide by 2. 1/2 = (m+1)/2 (mod m). */
	y = (y * (m+1)/2) % m;
	int z = w*t6 % m; /* w*v^3 */

	if (debug)
	  {
	    werror("addh: p: (%x, %x, %x)\n"
		   "q: (%x, %x, %x)\n"
		   "r: (%x, %x, %x)\n",
		   p->x, p->y, p->z,
		   q->x, q->y, q->z,
		   x, y, z);
	    werror("r1 = %x\n"
		   "r2 = %x\n"
		   "v = %x\n"
		   "s1 = %x\n"
		   "s2 = %x\n"
		   "u = %x\n"
		   "w = %x\n"
		   "t1 = %x\n"
		   "t2 = %x\n"
		   "t3 = %x\n"
		   "t4 = %x\n"
		   "t5 = %x\n"
		   "t6 = %x\n",
		   r1, r2, v, s1, s2, u,
		   w, t1, t2, t3, t4, t5, t6);
	  }

	return pointh(x, y, z);
      }
  }
  
  point normalize (pointh p)
  {
    if (!p->z)
      return point(0,0);
    else
      {
	int zinv = p->z->invert(m);
	return point (p->x * zinv % m, p->y * zinv % m);
      }
  }

  point mul(int k, point p)
  {
    if (k < 0)
      error("Negativ scalars not supported.\n");

    /* Right-to-left binary algorithm */
    point x = point(0,0);
    for (point power = p; k; k >>= 1, power = dup(power))
      {
	if (k & 1)
	  x = add(x, power);

#if 0
	write("x = %d\n"
	      "y = %d\n", x->x, x->y);
#endif
      }
    return x;
  }

  point mulh(int k, point p)
  {
    if (k < 0)
      error("Negativ scalars not supported.\n");

    /* Right-to-left binary algorithm */
    pointh x = pointh(0,1,0);

    for (point power = p->to_homogenous();
	 k; k >>= 1, power = duph(power))
      {
	if (k & 1)
	  x = addh(x, power);

#if 0
	write("x = %d\n"
	      "y = %d\n", x->x, x->y);
#endif
      }
    return normalize(x);
  }

  int valid_p (point p)
  {
    return (p->y * p->y - (p->x * p->x + a) * p->x ) % m == b;
  }
  int create(string name)
  {
    switch (name)
      {
      case "secp192r1":
	m = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF;
	a = -3;
	b = 0x64210519e59c80e70fa7e9ab72243049feb8deecc146b9b1;
	g = point(0x188da80eb03090f67cbf20eb43a18800f4ff0afd82ff1012,
		  0x07192b95ffc8da78631011ed6b24cdd573f977a11e794811);
	n = 0xffffffffffffffffffffffff99def836146bc9b1b4d22831;
	break;
      case "secp224r1":
	m = 0xffffffffffffffffffffffffffffffff000000000000000000000001;
	a = -3;
	b = 0xb4050a850c04b3abf54132565044b0b7d7bfd8ba270b39432355ffb4;
	g = point(0xb70e0cbd6bb4bf7f321390b94a03c1d356c21122343280d6115c1d21,
		  0xbd376388b5f723fb4c22dfe6cd4375a05a07476444d5819985007e34);
	n = 0xffffffffffffffffffffffffffff16a2e0b8f03e13dd29455c5c2a3d;


	break;
      case "secp256r1":
	m = 0xFFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF;
	a = -3;
	b = 0x5AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B;
	g = point(0x6B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296,
		  0x4FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5);
	n = 0xFFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551;
	break;
      case "secp384r1":
	m = 0xfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeffffffff0000000000000000ffffffff;
	a = -3;
	b = 0xb3312fa7e23ee7e4988e056be3f82d19181d9c6efe8141120314088f5013875ac656398d8a2ed19d2a85c8edd3ec2aef;
	g = point(0xaa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7,
		  0x3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f);
	n = 0xffffffffffffffffffffffffffffffffffffffffffffffffc7634d81f4372ddf581a0db248b0a77aecec196accc52973;
	break;
      case "secp521r1":
	m = 0x1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff;
	b = 0x051953eb9618e1c9a1f929a21a0b68540eea2da725b99b315f3b8b489918ef109e156193951ec7e937b1652c0bd3bb1bf073573df883d2c34f1ef451fd46b503f00;
	g = point(0xc6858e06b70404e9cd9e3ecb662395b4429c648139053fb521f828af606b4d3dbaa14b5e77efe75928fe1dc127a2ffa8de3348b3c1856a429bf97e7e31c2e5bd66,
		  0x11839296a789a3bc0045c8a5fb42c7d1bd998f54449579b446817afbd17273e662c97ee72995ef42640c550b9013fad0761353c7086a272c24088be94769fd16650);
	n = 0x1fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa51868783bf2f966b7fcc0148f709a5d03bb5c9b8899c47aebb6fb71e91386409;

	break;
      default:
	error("Unknown curve %s\n", name);
      }
  }
};

class exp_table
{
  curve ecc;
  array(point) table;
  int bit_size;
  int block_size;
  int nblocks;

  void create(curve c, int k)
  {
    ecc = c;
    nblocks = k;
    bit_size = ecc->m->size(2);
    block_size = (bit_size + nblocks-1) / nblocks;
    int table_size = 1 << nblocks;
    table = allocate(table_size);

    table[0] = point(0,0);

    /* First compute powers g^{2^{j block_size}} */
    table[1] = ecc->g;
    for (int j = 2; j < table_size; j *= 2)
      {
	table[j] = ecc->mul (1 << block_size, table[j/2]);
	for (int i = 1; i < j; i++)
	  table[j + i] = ecc->add(table[j], table[i]);
      }
  }
  int extract_column (array(int) m, int bit)
  {
    int c = 0;
    for (int i = 0; i < nblocks; i++)
      if (m[i] & bit)
	c |= (1 << i);
    return c;
  }

  point mul(int k)
  {    
    if (k->size(2) > bit_size)
      k %= ecc->n;
    
    array(int) e = allocate(nblocks);
    for (int i = 0; i < nblocks; i++)
      {
	e[i] = k % (1<<block_size);
	k >>= block_size;
      }
    int bit = 1 << (block_size - 1);

    point x = table[extract_column(e, bit)];
    while (bit >>= 1)
      {
	x = ecc->dup(x);
	x = ecc->add(x, table[extract_column(e, bit)]);
      }
    return x;
  }
}
