/* ecdsa.c */

/* nettle, low-level cryptographics library
 *
 * Copyright (C) 2013 Niels Möller
 *
 * The nettle library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The nettle library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the nettle library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02111-1301, USA.
 */

/* Development of Nettle's ECC support was funded by Internetfonden. */

#include <assert.h>
#include <stdlib.h>

#include "ecdsa.h"
#include "ecc-internal.h"
#include "gmp-glue.h"

/* Low-level ECDSA functions */

/* Like mpz_set_str, but always writes rn limbs. If input is larger,
   higher bits are ignored. */
static void
mpn_set_base256 (mp_limb_t *rp, mp_size_t rn,
		 const uint8_t *xp, size_t xn)
{
  size_t xi;
  mp_limb_t out;
  unsigned bits;
  for (xi = xn, out = bits = 0; xi > 0 && rn > 0; )
    {
      mp_limb_t in = xp[--xi];
      out |= (in << bits) & GMP_NUMB_MASK;
      bits += 8;
      if (bits >= GMP_NUMB_BITS)
	{
	  *rp++ = out;
	  rn--;

	  bits -= GMP_NUMB_BITS;
	  out = in >> (8 - bits);
	}
    }
  if (rn > 0)
    {
      *rp++ = out;
      if (--rn > 0)
	mpn_zero (rp, rn);
    }
}

static int
zero_p (const struct ecc_curve *ecc,
	const mp_limb_t *xp)
{
  mp_limb_t t;
  mp_size_t i;

  for (i = t = 0; i < ecc->size; i++)
    t |= xp[i];

  return t == 0;
}

static int
ecdsa_in_range (const struct ecc_curve *ecc,
		const mp_limb_t *xp, mp_limb_t *scratch)
{
  /* Check that 0 < x < q, with data independent timing. */
  return !zero_p (ecc, xp)
    && mpn_sub_n (scratch, xp, ecc->q, ecc->size) != 0;
}

/* Generate a random number in the range, 0 < x < group order. Timing
   is non-determinstic, but independent of the returned value. */
#define ECDSA_RANDOM_ITCH(size) (size)

static void
ecdsa_random (const struct ecc_curve *ecc, mp_limb_t *xp,
	      void *ctx, nettle_random_func *random, mp_limb_t *scratch)
{
  uint8_t *buf = (uint8_t *) scratch;
  unsigned nbytes = (ecc->bit_size + 7)/8;

  /* The bytes ought to fit in the scratch area, unless we have very
     unusual limb and byte sizes. */
  assert (nbytes <= ecc->size * sizeof (mp_limb_t));

  do
    {
      /* q and p are of the same bitsize. */
      random (ctx, nbytes, buf);
      buf[0] &= 0xff >> (nbytes * 8 - ecc->bit_size);

      mpn_zero (xp, ecc->size);
      mpn_set_base256 (xp, ecc->size, buf, nbytes);
    }
  while (!ecdsa_in_range (ecc, xp, scratch));
}

/* Convert hash value to an integer. If the digest is larger than
   the ecc bit size, then we must truncate it and use the leftmost
   bits. */
/* NOTE: We don't considered the hash value to be secret, so it's ok
   if the running time of this conversion depends on h.

   Requires ecc->size + 1 limbs, the extra limb may be needed, for
   unusual limb sizes.
*/
static void
ecdsa_hash_to_mpn (const struct ecc_curve *ecc,
		   mp_limb_t *hp,
		   unsigned length, const uint8_t *digest)
{
  if (length > ((unsigned) ecc->bit_size + 7) / 8)
    length = (ecc->bit_size + 7) / 8;

  mpn_set_base256 (hp, ecc->size + 1, digest, length);

  if (8 * length > ecc->bit_size)
    /* We got a few extra bits, at the low end. Discard them. */
    mpn_rshift (hp, hp, ecc->size + 1, 8*length - ecc->bit_size);
}

mp_size_t
_ecdsa_sign_itch (const struct ecc_curve *ecc)
{
  /* Needs 3*ecc->size + scratch for ecc_mul_g. */
  return _ECDSA_SIGN_ITCH (ecc->size);
}

/* NOTE: Caller should check if r or s is zero. */
void
_ecdsa_sign (const struct ecc_curve *ecc,
	     const mp_limb_t *zp,
	     /* Random nonce, must be invertible mod ecc group
		order. */
	     const mp_limb_t *kp,
	     unsigned length, const uint8_t *digest,
	     mp_limb_t *rp, mp_limb_t *sp,
	     mp_limb_t *scratch)
{
  mp_limb_t cy;
#define P	    scratch
#define kinv	    scratch                /* Needs 5*ecc->size for computation */
#define hp	    (scratch  + ecc->size) /* NOTE: ecc->size + 1 limbs! */
#define tp	    (scratch + 2*ecc->size)
  /* Procedure, according to RFC 6090, "KT-I". q denotes the group
     order.

     1. k <-- uniformly random, 0 < k < q

     2. R <-- (r_x, r_y) = k g

     3. s1 <-- r_x mod q

     4. s2 <-- (h + z*s1)/k mod q.
  */

  ecc_mul_g (ecc, P, kp, P + 3*ecc->size);
  /* x coordinate only */
  ecc_j_to_a (ecc, 3, rp, P, P + 3*ecc->size);

  /* We need to reduce x coordinate mod ecc->q. It should already
     be < 2*ecc->q, so one subtraction should suffice. */
  cy = mpn_sub_n (scratch, rp, ecc->q, ecc->size);
  cnd_copy (cy == 0, rp, scratch, ecc->size);

  /* Invert k, uses 5 * ecc->size including scratch */
  mpn_copyi (hp, kp, ecc->size);
  ecc_modq_inv (ecc, kinv, hp, tp);
  
  /* Process hash digest */
  ecdsa_hash_to_mpn (ecc, hp, length, digest);

  ecc_modq_mul (ecc, tp, zp, rp);
  ecc_modq_add (ecc, hp, hp, tp);
  ecc_modq_mul (ecc, tp, hp, kinv);

  mpn_copyi (sp, tp, ecc->size);
#undef P
#undef hp
#undef kinv
#undef tp
}

mp_size_t
_ecdsa_verify_itch (const struct ecc_curve *ecc)
{
  /* Largest storage need is for the ecc_mul_a call, 6 * ecc->size +
     ECC_MUL_A_ITCH (size) */
  return _ECDSA_VERIFY_ITCH (ecc->size);
}

int
_ecdsa_verify (const struct ecc_curve *ecc,
	       const mp_limb_t *pp, /* Public key */
	       unsigned length, const uint8_t *digest,
	       const mp_limb_t *rp, const mp_limb_t *sp,
	       mp_limb_t *scratch)
{
  /* Procedure, according to RFC 6090, "KT-I". q denotes the group
     order.

     1. Check 0 < r, s < q.

     2. s' <-- s^{-1}  (mod q)

     3. u1  <-- h * s' (mod q)

     4. u2  <-- r * s' (mod q)

     5. R = u1 G + u2 Y

     6. Signature is valid if R_x = r (mod q).
  */

#define P2 scratch
#define P1 (scratch + 3*ecc->size)
#define sinv (scratch + 3*ecc->size)
#define u2 (scratch + 4*ecc->size)
#define hp (scratch + 4*ecc->size)
#define u1 (scratch + 6*ecc->size)

  if (! (ecdsa_in_range (ecc, rp, scratch)
	 && ecdsa_in_range (ecc, sp, scratch)))
    return 0;

  /* FIXME: Micro optimizations: Either simultaneous multiplication.
     Or convert to projective coordinates (can be done without
     division, I think), and write an ecc_add_ppp. */
  
  /* Compute sinv, use P2 as scratch */
  mpn_copyi (sinv + ecc->size, sp, ecc->size);
  ecc_modq_inv (ecc, sinv, sinv + ecc->size, P2);

  /* u2 = r / s, P2 = u2 * Y */
  ecc_modq_mul (ecc, u2, rp, sinv);

   /* Total storage: 5*ecc->size + ECC_MUL_A_ITCH (ecc->size) */
  ecc_mul_a (ecc, 1, P2, u2, pp, u2 + ecc->size);

  /* u1 = h / s, P1 = u1 * G */
  ecdsa_hash_to_mpn (ecc, hp, length, digest);
  ecc_modq_mul (ecc, u1, hp, sinv);

  /* u = 0 can happen only if h = 0 or h = q, which is extremely
     unlikely. */
  if (!zero_p (ecc, u1))
    {
      /* Total storage: 6*ecc->size + ECC_MUL_G_ITCH (ecc->size) */
      ecc_mul_g (ecc, P1, u1, u1 + ecc->size);

      /* NOTE: ecc_add_jjj and/or ecc_j_to_a will produce garbage in
	 case u1 G = +/- u2 V. However, anyone who gets his or her
	 hands on a signature where this happens during verification,
	 can also get the private key as z = +/- u1 / u_2 (mod q). And
	 then it doesn't matter very much if verification of
	 signatures with that key succeeds or fails.

	 u1 G = - u2 V can never happen for a correctly generated
	 signature, since it implies k = 0.

	 u1 G = u2 V is possible, if we are unlucky enough to get h /
	 s_1 = z. Hitting that is about as unlikely as finding the
	 private key by guessing.
       */
      /* Total storage: 6*ecc->size + ECC_ADD_JJJ_ITCH (ecc->size) */
      ecc_add_jjj (ecc, P1, P1, P2, u1);
    }
  ecc_j_to_a (ecc, 3, P2, P1, u1);

  if (mpn_cmp (P2, ecc->q, ecc->size) >= 0)
    mpn_sub_n (P2, P2, ecc->q, ecc->size);

  return (mpn_cmp (rp, P2, ecc->size) == 0);
#undef P2
#undef P1
#undef sinv
#undef u2
#undef hp
#undef u1
}


/* Implementation of the high-level ECDSA functions */
static mp_limb_t *
alloc_limbs_as_gmp (mp_size_t n)
{

  void *(*alloc_func)(size_t);

  assert (n > 0);

  mp_get_memory_functions (&alloc_func, NULL, NULL);
  return (mp_limb_t *) alloc_func ( (size_t) n * sizeof(mp_limb_t));
}

static void
free_limbs_as_gmp (mp_limb_t *p, mp_size_t n)
{
  void (*free_func)(void *, size_t);
  assert (n > 0);
  assert (p != 0);
  mp_get_memory_functions (NULL, NULL, &free_func);

  free_func (p, (size_t) n * sizeof(mp_limb_t));
}

static void
_mpz_set_mpn (mpz_t r, const mp_limb_t *xp, mp_size_t xn)
{
  mpn_copyi (_mpz_write_limbs (r, xn), xp, xn);
  _mpz_done_limbs (r, xn);
}

void
ecdsa_public_key_init (struct ecdsa_public_key *pub,
		       const struct ecc_curve *ecc)
{
  pub->ecc = ecc;
  pub->Y = alloc_limbs_as_gmp (2*ecc->size);
}

void
ecdsa_public_key_clear (struct ecdsa_public_key *pub)
{
  free_limbs_as_gmp (pub->Y, 2*pub->ecc->size);
}

int
ecdsa_set_public_key (struct ecdsa_public_key *pub,
		      const mpz_t x, const mpz_t y)
{
  mp_size_t size;  
  mpz_t lhs, rhs;
  mpz_t t;
  int res;

  size = pub->ecc->size;
  
  if (mpz_sgn (x) < 0 || _mpz_cmp_limbs (x, pub->ecc->p, size) >= 0
      || mpz_sgn (y) < 0 || _mpz_cmp_limbs (y, pub->ecc->p, size) >= 0)
    return 0;

  mpz_init (lhs);
  mpz_init (rhs);

  /* Check that y^2 = x^3 - 3*x + b (mod p) */
  mpz_mul (lhs, y, y);
  mpz_mul (rhs, x, x);
  mpz_sub_ui (rhs, rhs, 3);
  mpz_mul (rhs, rhs, x);
  mpz_add (rhs, rhs, _mpz_init_mpn (t, pub->ecc->b, size));

  res = mpz_congruent_p (lhs, rhs, _mpz_init_mpn (t, pub->ecc->p, size));

  mpz_clear (lhs);
  mpz_clear (rhs);

  if (!res)
    return 0;

  _mpz_copy_limbs (pub->Y, x, size);
  _mpz_copy_limbs (pub->Y + size, y, size);

  return 1;
}

void
ecdsa_get_public_key (struct ecdsa_public_key *pub,
		      mpz_t x, mpz_t y)
{
  mp_size_t size = pub->ecc->size;
  _mpz_set_mpn (x, pub->Y, size);
  _mpz_set_mpn (y, pub->Y + size, size);
}

void
ecdsa_private_key_init (struct ecdsa_private_key *key,
			const struct ecc_curve *ecc)
{
  key->ecc = ecc;
  key->z = alloc_limbs_as_gmp (ecc->size);
}

void
ecdsa_private_key_clear (struct ecdsa_private_key *key)
{
  free_limbs_as_gmp (key->z, key->ecc->size);
}

int
ecdsa_set_private_key (struct ecdsa_private_key *key,
		       const mpz_t z)
{
  mp_size_t size = key->ecc->size;

  if (mpz_sgn (z) <= 0 || _mpz_cmp_limbs (z, key->ecc->q, size) >= 0)
    return 0;

  _mpz_copy_limbs (key->z, z, size);
  return 1;
}

void
ecdsa_get_private_key (struct ecdsa_private_key *key,
		       mpz_t z)
{
  _mpz_set_mpn (z, key->z, key->ecc->size);
}

void
ecdsa_sign (const struct ecdsa_private_key *key,
	    void *random_ctx, nettle_random_func *random,
	    unsigned digest_length,
	    const uint8_t *digest,
	    struct dsa_signature *signature)
{
  mp_limb_t size = key->ecc->size;
  /* At most 936 bytes. */
  mp_limb_t *k = TMP_ALLOC_LIMBS (size + _ECDSA_SIGN_ITCH (size));
  mp_limb_t *rp = _mpz_write_limbs (signature->r, size);
  mp_limb_t *sp = _mpz_write_limbs (signature->s, size);
  
  /* Timing reveals the number of rounds through this loop, but the
     timing is still independent of the secret k finally used. */
  do
    {
      ecdsa_random (key->ecc, k, random_ctx, random, k + size);
      _ecdsa_sign (key->ecc, key->z, k, digest_length, digest,
		   rp, sp, k + size);
      _mpz_done_limbs (signature->r, size);
      _mpz_done_limbs (signature->s, size);
    }
  while (mpz_sgn (signature->r) == 0 || mpz_sgn (signature->s) == 0);
}

int
ecdsa_verify (const struct ecdsa_public_key *pub,
	      unsigned length, const uint8_t *digest,
	      const struct dsa_signature *signature)
{
  mp_limb_t size = pub->ecc->size;
  mp_size_t itch = 2*size + _ECDSA_VERIFY_ITCH (size);
  /* For ECC_MUL_A_WBITS == 0, at most 1512 bytes. With
     ECC_MUL_A_WBITS == 4, currently needs 67 * ecc->size, at most
     4824 bytes. Don't use stack allocation for this. */
  mp_limb_t *scratch = alloc_limbs_as_gmp (itch);
  int res;

#define rp scratch
#define sp (scratch + size)
#define scratch_out (scratch + 2*size)

  if (mpz_sgn (signature->r) <= 0 || mpz_size (signature->r) > size
      || mpz_sgn (signature->s) <= 0 || mpz_size (signature->s) > size)
    return 0;
  
  _mpz_copy_limbs (rp, signature->r, size);
  _mpz_copy_limbs (sp, signature->s, size);

  res = _ecdsa_verify (pub->ecc, pub->Y, length, digest, rp, sp, scratch_out);

  free_limbs_as_gmp (scratch, itch);

  return res;
#undef rp
#undef sp
#undef scratch_out
}

void
ecdsa_generate_keypair (struct ecdsa_public_key *pub,
			struct ecdsa_private_key *key,
			void *random_ctx, nettle_random_func *random)
{
  mp_size_t itch = 3*pub->ecc->size + ECC_MUL_G_ITCH (pub->ecc->size);
  mp_limb_t *p = TMP_ALLOC_LIMBS (itch);
  
  ecdsa_random (pub->ecc, key->z, random_ctx, random, p);
  ecc_mul_g (pub->ecc, p, key->z, p + 3*pub->ecc->size);
  ecc_j_to_a (pub->ecc, 1, pub->Y, p, p + 3*pub->ecc->size);
}
