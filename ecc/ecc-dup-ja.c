/* ecc-dup-ja.c */

/* nettle, low-level cryptographics library
 *
 * Copyright (C) 2013 Niels Möller
 *  
 * The nettle library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 * 
 * The nettle library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the nettle library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02111-1301, USA.
 */

/* Development of Nettle's ECC support was funded by Internetfonden. */

#include "ecc-internal.h"

mp_size_t
ecc_dup_ja_itch (const struct ecc_curve *ecc)
{
  return ECC_DUP_JA_ITCH (ecc->size);
}

void
ecc_dup_ja (const struct ecc_curve *ecc,
	    mp_limb_t *r, const mp_limb_t *p,
	    mp_limb_t *scratch)
{
  /* Formulas (from djb,
     http://www.hyperelliptic.org/EFD/g1p/auto-shortw-jacobian-3.html#doubling-mdbl-2007-bl
     Operation			Live variables
     XX = x^2			XX
     YY = y^2			XX, YY
     YYYY = YY^2		XX, YY, YYYY
     S = 2*((X+YY)^2-XX-YYYY)   XX, YYYY, S	!YY
     M = 3*XX - 3		YYYY, S, M	!XX
     z' = 2*y_1
     x' = M^2-2*S
     y' = M*(S-x')-8*YYYY     
  */

#define x2 scratch
#define y2 (scratch + ecc->size)
#define y4 (scratch + 2*ecc->size)
#define S (scratch + 3*ecc->size)
#define M (scratch + 4*ecc->size)
#define sp scratch

#define xp p
#define yp (p + ecc->size)

  /* z' */
  ecc_modp_mul_1 (ecc, r + 2*ecc->size, yp, 2);

  /* S */
  ecc_modp_sqr (ecc, x2, xp);
  ecc_modp_sqr (ecc, y2, yp);
  ecc_modp_sqr (ecc, y4, y2);
  ecc_modp_add (ecc, y2, xp, y2);
  ecc_modp_sqr (ecc, S, y2);
  ecc_modp_sub (ecc, S, S, x2);
  ecc_modp_sub (ecc, S, S, y4);
  ecc_modp_mul_1 (ecc, S, S, 2);

  /* M */
  ecc_modp_sub (ecc, M, x2, ecc->unit);
  ecc_modp_mul_1 (ecc, M, M, 3);

  /* x' */
  ecc_modp_sqr (ecc, sp, M);
  ecc_modp_submul_1 (ecc, sp, S, 2);
  mpn_copyi (r, sp, ecc->size);

  /* y' */
  ecc_modp_sub (ecc, S, S, r);
  ecc_modp_mul (ecc, sp, S, M);
  ecc_modp_submul_1 (ecc, sp, y4, 8);
  mpn_copyi (r + ecc->size, sp, ecc->size);
}
