/* ecdsa.h */

/* nettle, low-level cryptographics library
 *
 * Copyright (C) 2013 Niels Möller
 *  
 * The nettle library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 * 
 * The nettle library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the nettle library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02111-1301, USA.
 */

/* Development of Nettle's ECC support was funded by Internetfonden. */

#ifndef NETTLE_ECDSA_H_INCLUDED
#define NETTLE_ECDSA_H_INCLUDED

#include <nettle/dsa.h>

#include "ecc.h"

/* High-level ECDSA interface. Uses mpz interface, and allocates
   storage using the same allocation functions as GMP. */
struct ecdsa_public_key
{
  const struct ecc_curve *ecc;
  /* Allocated using the same allocation function as GMP. */
  mp_limb_t *Y;
};

struct ecdsa_private_key
{
  const struct ecc_curve *ecc;
  /* Size ecc->size. */
  mp_limb_t *z;
};

void
ecdsa_public_key_init (struct ecdsa_public_key *pub,
		       const struct ecc_curve *ecc);
void
ecdsa_public_key_clear (struct ecdsa_public_key *pub);

int
ecdsa_set_public_key (struct ecdsa_public_key *pub,
		      const mpz_t x, const mpz_t y);
void
ecdsa_get_public_key (struct ecdsa_public_key *pub,
		      mpz_t x, mpz_t y);

void
ecdsa_private_key_init (struct ecdsa_private_key *key,
			const struct ecc_curve *ecc);
void
ecdsa_private_key_clear (struct ecdsa_private_key *key);

int
ecdsa_set_private_key (struct ecdsa_private_key *key,
		       const mpz_t z);
void
ecdsa_get_private_key (struct ecdsa_private_key *key,
		       mpz_t z);

void
ecdsa_sign (const struct ecdsa_private_key *key,
	    void *random_ctx, nettle_random_func *random,
	    unsigned digest_length,
	    const uint8_t *digest,
	    struct dsa_signature *signature);

int
ecdsa_verify (const struct ecdsa_public_key *pub,
	      unsigned length, const uint8_t *digest,
	      const struct dsa_signature *signature);

void
ecdsa_generate_keypair (struct ecdsa_public_key *pub,
			struct ecdsa_private_key *key,
			void *random_ctx, nettle_random_func *random);

/* Low-level ECDSA interface. */

mp_size_t
_ecdsa_sign_itch (const struct ecc_curve *ecc);
void
_ecdsa_sign (const struct ecc_curve *ecc,
	     /* Private key */
	     const mp_limb_t *zp,
	     /* Random nonce, must be invertible mod ecc group
		order. */
	     const mp_limb_t *kp,
	     unsigned length, const uint8_t *digest,
	     mp_limb_t *rp, mp_limb_t *sp, mp_limb_t *scratch);


mp_size_t
_ecdsa_verify_itch (const struct ecc_curve *ecc);
int
_ecdsa_verify (const struct ecc_curve *ecc,
	       const mp_limb_t *pp, /* Public key */
	       unsigned length, const uint8_t *digest,
	       const mp_limb_t *rp, const mp_limb_t *sp,
	       mp_limb_t *scratch);

#endif /* NETTLE_ECDSA_H_INCLUDED */
