#! /usr/bin/pike

void output (string s, object p)
{
  write("%s:\n\"%x\",\n\"%x\"\n\n", s, p->x, p->y);
};

int main (int argc, array(string) argv)
{
  object c = .ecc.curve(argv[1]);

  object g2 = c->dup(c->g);
  object g3 = c->add(c->g, g2);
  object g4 = c->dup(g2);
  object g4a = c->add(c->g, g3);

  output ("g2", g2);
  output ("g3", g3);
  output ("g4", g4);
  output ("g41", g4a);

  int z = 1 + random(c->n - 1);
  object P = c->mul (z, c->g);

  output ("pub", P);
  write("priv:\n\"%x\"\n", z);
  return 0;
}
