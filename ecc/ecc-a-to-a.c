/* ecc-a-to-a.c */

/* nettle, low-level cryptographics library
 *
 * Copyright (C) 2013 Niels Möller
 *  
 * The nettle library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 * 
 * The nettle library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the nettle library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02111-1301, USA.
 */

/* Development of Nettle's ECC support was funded by Internetfonden. */

#include <assert.h>

#include "ecc-internal.h"

mp_size_t
ecc_a_to_a_itch (const struct ecc_curve *ecc)
{
  return ecc->use_redc ? 2*ecc->size : 0;
}

void
ecc_a_to_a (const struct ecc_curve *ecc,
	    mp_limb_t *r, const mp_limb_t *p,
	    mp_limb_t *scratch)
{
  if (ecc->use_redc)
    {
      /* Convert y coordinate, put at t. */
      mpn_zero (scratch, ecc->size);
      mpn_copyi (scratch + ecc->size, p + ecc->size, ecc->size);
      ecc->modp (ecc, scratch);
      /* Convert x coordiante, put at r. If p == r, clobbers all of
	 p. */
      mpn_copyi (r + ecc->size, p, ecc->size);
      mpn_zero (r, ecc->size);
      ecc->modp (ecc, r);

      /* Copy y coordinate */
      mpn_copyi (r + ecc->size, scratch, ecc->size);      
    }
  else if (r != p)
    mpn_copyi (r, p, 2*ecc->size);
}

